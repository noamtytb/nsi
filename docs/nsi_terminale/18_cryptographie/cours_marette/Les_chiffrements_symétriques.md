# Les chiffrements symétriques



## I. Le code César

Lors de ses batailles, l’empereur romain JULES CÉSAR cryptait les messages qu’il envoyait à ses généraux. Sa méthode de codage consistait à décaler les lettres de 3 rangs, vers la droite, dans l’alphabet.
Cette méthode de cryptage est appelée chiffrement de César, ou Code César.
Le nombre de rangs de décalage des lettres est appelé la clé. (JULES CÉSAR employait donc la clé égale à 3



Pour l'exercice nous prendrons l'alphabet français en lettre capitale comme alphabet de référence.

On peut coder un message à l’aide du code César avec n’importe quelle clé n, où n est un entier naturel.

1. Quelle condition vérifie cependant l’entier n ?



2. Crypter la lettre C à l’aide du code César avec clé 17.

   

3.  Decrypter la lettre M à l’aide du code César avec clé 17.



4. Vous allez maintenant envoyer un message à l'un de vos camarades.

   Choisissez une clé et écrivez la sur le papier à lettre.

   Ecrivez votre message en clair sur la première ligne du papier de brouillon.

   A l'aide du disque de codage, codez votre message sur la seconde ligne. On ne code que les caractères présents dans l'alphabet, les autres restent inchangés.

   Recopiez le chiffré sur le papier à lettre pliez ce papier. Faites le transitter jusqu'au destinataire sans vous lever en vous "servant" de vos voisins.

5. Après avoir reçu un message, répondez à votre expéditeur avec la même clé.

6. Récupérez le fichier python codage_symetrique.py

7. Dans ce fichier l'alphabet est stocké dans la variable globale ALPHABET. On rappel que le résultat de l'opération a%b est le reste de la division euclidienne de a par b. Complétez le code de la méthode **coder_cesar(clair,cle)** qui prend deux paramètres, le clair à coder et la cle sous forme d'entier. Cette fonction renvoie le clair coder à l'aide de la méthode du code de césar avec un décalage de cle.

8. Ecrivez le corps de la méthode **decoder_cesar(chiffre,cle)** qui prend deux paramètres, le chiffré à décoder et la cle sous forme d'entier. Cette fonction renvoie le chiffré décoder à l'aide de la méthode du code de césar avec un décalage de cle.

Par analyse du texte, il est possible de retrouver très facilement la clé. Par exemple en français, le caractère le plus présent est le E. Il suffit de regarder quel est le caractère le plus présent dans le chiffré et de lui associer le E dans le clair pour découvrir la clé. Cette méthode est d'autant plus fiable que le chiffré est long.





## II. Le chiffrement de vigenère

 L’idée de Vigenère est d’utiliser un chiffre de César, mais où le décalage utilisé change de lettres en lettres. Pour cela, on utilise une table composée de 26 alphabets, écrits dans l’ordre, mais décalés de ligne en ligne d’un caractère. On écrit encore en haut un alphabet complet, pour le texte à coder, et à gauche, verticalement, un dernier alphabet, pour la clé. Il sagit de la table de vigenère

## Principe de chiffrement

A chaque lettre en clair, on sélectionne la colonne correspondante tandis que la lettre de la clé se sélectionne par ligne, au croisement de la ligne et de la colonne on trouve la lettre chiffrée.
Texte en clair : DECODER C EST GENIAL
Clé répétée : CLE
La première lettre vaut : Colonne D, ligne C : on obtient la lettre F.
La deuxième lettre vaut : Colonne E, ligne L : on obtient la lettre P.
La troisième lettre vaut : Colonne C, ligne E : on obtient la lettre G.
La quatrième lettre vaut : Colonne O, ligne C : on obtient la lettre Q



1. Poursuivez le chiffrement.



## Principe du déchiffrement 

On regarde pour chaque lettre de la clé répétée, la ligne correspondante sur laquelle on cherche la lettre chiffrée. Le nom de la colonne donne la lettre déchiffrée.
Texte chiffré : 'XZYU LPNPD VZYU LZQTV NP FCN'
Clé répétée : CLE
La première lettre vaut : Colonne C, on cherche X : on trouve la colonne V.
La deuxième lettre vaut : Colonne L, on cherche Z : on trouve la colonne O.
La troisième lettre vaut : Colonne E, on cherche Y : on trouve la colonne U.
La quatrième lettre vaut : Colonne C, on cherche U : on trouve la colonne S.

2. Poursuivez le décodage



3. Vous allez maintenant envoyer un message à l'un de vos camarades.

   De la même manière que pour le code de césar, Choisissez une clé et écrivez la sur le papier à lettre.

   Ecrivez votre message en clair sur la première ligne du papier de brouillon.

   A l'aide de la table de Vignère , codez votre message sur la seconde ligne. On ne code que les caractères présents dans l'alphabet, les autres restent inchangés.

   Recopiez le chiffré sur le papier à lettre pliez ce papier. Faites le transitter jusqu'au destinataire sans vous lever en vous "servant" de vos voisins.

5. Après avoir reçu un message, répondez à votre expéditeur avec la même clé.

7.  Reprenez le fichier codage_symétrique.py. Complétez le code de la méthode **chiffrer_vigenere(clair,cle)** qui prend deux paramètres, le clair à coder et la cle sous forme de chaine de caractères. Cette fonction renvoie le clair chiffrer à l'aide de la méthode du chiffrement de vigenère avec un décalage de cle.

8. Complétez le corps de la méthode **dechiffrer_vigenere(chiffre,cle)** qui prend deux paramètres, le chiffré à décoder et la cle sous forme de chaine de caractères. Cette fonction renvoie le chiffré décoder à l'aide de la méthode du  chiffrement de vigenère  avec un décalage de cle.

