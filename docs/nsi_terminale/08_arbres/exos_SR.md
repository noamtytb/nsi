# **Structure de données : les arbres binaires**

## **Exercices d'applications**

Pour tous les arbres, la profondeur du noeud racine est est de 0.

![arbres_1](./arbres_1.png)

**Question 1** - Remplir le tableau suivant avec les arbres ci-dessus :

|               |arbre 1|arbre 2|arbre 3|
|:-------------:|:-----:|:-----:|:-----:|
|taille         |       |       |       |
|profondeur du noeud d'étiquette 1 |       |       |       |
|profondeur du noeud d'étiquette 2 |       |       |       |
|profondeur du noeud d'étiquette 5 |       |       |       |
|profondeur du noeud d'étiquette 7 |       |       |       |
|profondeur du noeud d'étiquette 10|       |       |       |
|profondeur du noeud d'étiquette 13|       |       |       |
|profondeur du noeud d'étiquette 14|       |       |       |
|hauteur        |       |       |       |

---


**Question 2** - Donner les chemins depuis à la racine pour atteindre le noeud étiqueté 13.


---


**Question 3** - Caractériser les 5 arbres suivants.

![arbres_3](./arbres_3.png)


---


**Question 4** - Donner l'encadrement de la taille d'un arbre de hauteur 10.


---

**Question 5** - Donner l'encadrement de la hauteur d'un arbre de taille 56.
