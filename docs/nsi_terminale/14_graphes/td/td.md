# **Structure de données : Graphes**

## **Quelques exercices**

### **Question 1 - Graphe non orienté**

Construisez un graphe de réseau social à partir des informations suivantes :

- Arthur est ami avec Benoit et Ellodie,
- Benoit est ami avec Arthur et Coralie,
- Coralie est ami avec Benoit, Franck et David,
- David est ami avec Coralie, Franck et Ellodie,
- Ellodie est ami avec Arthur, David et Franck,
- Franck est ami avec Coralie, David et Ellodie.

---

### **Question 2 - Graphe orienté**

Eva décide de faire un graphe représentant les différentes ruelles de son village. On y trouve une boulangerie, une école, un bureau de poste, une boucherie, une mairie, une église et une salle des fêtes. Certaines ruelles sont à double sens et d'autres à sens unique.

Eva décide de donner pour chacune des arêtes de son graphe une valeur qui correpond au temps qu'elle met pour traverser la ruelle à pied (chaque arête représente un sens de circulation).

Voici ses données :
- il y a une ruelle entre la boulangerie et le bureau de poste (double sens) - 2 minutes,
- il y a une ruelle entre la boulangerie et l'école (sens unique de l'école vers la boulangerie) - 3 minutes,
- il y a une ruelle entre la boulangerie et la boucherie (sens unique de la boulangerie vers la boucherie) - 4 minutes,
- il y a une ruelle entre l'école et l'église (sens unique de l'école vers l'église) - 3 minutes,
- il y a une ruelle entre l'école et la mairie (sens unique de la mairie vers l'école) - 4 minutes,
- il y a une ruelle entre l'école et la salle des fêtes (double sens) - 6 minutes,
- il y a une ruelle entre la boucherie et la salle des fêtes (double sens) - 5 minutes,
- il y a une ruelle entre la mairie et l'église (double sens) - 7 minutes.

Dessinez le graphe représentant le village d'Eva.

---

### **Question 3 - Arêtes, ordre et degré**

![graphe question 3](./graph_question3.PNG)

Donnez l'ordre du graphe ci dessus ainsi que le degré de **chaque** sommet.

---

### **Question 4 - Chaînes**

Sur le graphe de la **Question 3**, donnez s'il en existe une chaîne hamiltonienne **et** une chaîne eulérienne.

---

### **Question 5 - Représentation et Matrice d'adjacence**

![graphe question 5](./graph_question5.PNG)

Représentez par un dictionnaire le graphe ci dessus puis donner sa matrice d'adjacence.

---

### **Question 6 - Distance**

Voici le résultat de l'élévation de la matrice d'adjacence de la **question 5** à la puissance 3 :

![matrice d'adjacence à la puissance 3](./matrice_pui_3.PNG)

Que peut on dire des sommets 5 et 2 ?