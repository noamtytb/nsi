# **TP 2 - Graphes ~ Matrice**

## **Représentation d'une matrice en python**

Une manière de représenter une matrice en python est d'utiliser une liste de liste :

Ainsi la matrice M ci dessous
```
1   2   3
3   2   2
1   2   4
```

peut être représenté comme ceci :

```python
M = [
    [1, 2, 3],
    [3, 2, 2],
    [1, 2, 4]
]
```

## **Multiplication entre 2 matrices**

### **Un conseil**

Lorsque l'on souhaite faire la multiplication entre 2 matrices, il est conseillé de mettre la première matrice au dessus de la matrice résultat et la seconde matice à gauche de la matrice résultat (exemple ci dessous).

![résultat](./resultat.PNG)

### **Le calcul**

La multiplication entre 2 matrices consiste à faire la somme de la multiplication entre les éléments de la ligne de la première matrice avec les éléments de la colonne de la seconde matrice.

Par exemple l'élément à la position tout en haut à gauche de la matrice résultat (à la position M\[0]\[0]), ce calcul comme ceci :

![calcul](./resultat_schema.PNG)

### **Algorithme**

Le but de ce TP consiste à réaliser un algorithme permettant de calculer l'élévation au carré d'une matrice.

Avant de se lancer dans l'écriture de la fonction, il faut savoir par ou commencer :

Voyons sur une matrice 3x3 les calculs que nous devons réaliser (nous garderons la matrice exemple du début).

```python
Matrice_res = [
    [
        M[0][0]*M[0][0] + M[1][0]*M[0][1] + M[2][0]*M[0][2],
        M[0][1]*M[0][0] + M[1][1]*M[0][1] + M[2][1]*M[0][2],
        M[0][2]*M[0][0] + M[1][2]*M[0][1] + M[2][2]*M[0][2]
    ],
    [
        M[0][0]*M[1][0] + M[1][0]*M[1][1] + M[2][0]*M[1][2],
        M[0][1]*M[1][0] + M[1][1]*M[1][1] + M[2][1]*M[1][2],
        M[0][2]*M[1][0] + M[1][2]*M[1][1] + M[2][2]*M[1][2]
    ],
    [
        M[0][0]*M[2][0] + M[1][0]*M[2][1] + M[2][0]*M[2][2],
        M[0][1]*M[2][0] + M[1][1]*M[2][1] + M[2][1]*M[2][2],
        M[0][2]*M[2][0] + M[1][2]*M[2][1] + M[2][2]*M[2][2]
    ]
]
```

Après analyse des calculs, on remarque que nous pouvons faire le calcul avec 3 boucles ```for``` :

- une première boucle pour faire chaque ligne,
- une seconde pour les colonnes,
- et une dernière pour les 3 valeurs sur chaque ligne.

Voici le programme :

```python
def carre(m):
    res = []
    for i in range(len(m[0])):
        tmp = []
        for j in range(len(m[0])):
            val = 0
            for k in range(len(m[0])):
                val = val + m[k][j] * m[i][k]
            tmp.append(val)
        res.append(tmp)
    return res
```