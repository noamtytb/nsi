class Graphe_oriente_liste() :
    '''
    une classe pour les graphes orientés construits avec un dictionnaire d'adjacence
    '''
    def __init__(self):
        '''
        construit le graphe avec pour seul attribut un dictionnaire vide
        '''
        pass
    
    def ajouter_noeud(self, noeud):
        '''
        ajoute une clé sommet au dictionnaire d'adjacence
        : param noeud un noeud
        type ?
        : Pas de return, EFFET DE BORD sur self
        '''
        pass
            
    def renvoyer_noeuds(self):
        '''
        renvoie la liste des noeuds
        : return
        type list
        '''
        liste_sommets = []
        """
        for cle in a_completer:
            liste_sommets.append(a_completer)
        """
        pass
  
    
    def ajouter_arc(self, noeud1, noeud2) :
        '''
        ajoute un arc orienté de noeud1 vers noeud2
        : param noeud1, noeud2 deux noeuds existants ou non.
        type int
        : pas de return, EFFET DE BORD sur self
        '''
        pass
        
      
    def a_arc(self, noeud1, noeud2) :
        '''
        renvoie True si il y a un arc orienté de noeud1 vers noeud2 et False sinon
         : param noeud1, noeud2 deux nom de noeud 
        type any
        : return
        type boolean
        '''
        pass
        
    def voisins(self, noeud):
        '''
        renvoie la liste des voisins du noeuds
        : param noeud
        type any
        :return
        type list
        '''
        pass
    
    def __str__(self) :
        '''
        renvoie une chaîne pour l'affichage dans la console
        : return
        type str
        '''
        pass
        
    def __repr__(self):
        '''
        renvoie une chaine pour décrir l'objet
        '''
        pass
    
    

        
                
        
        
    
class Graphe_non_oriente_liste(Graphe_oriente_liste):
    '''
    une classe pour les graphes non orientés avec dictionnaire d'adjacence
    '''
    def __init__(self):
        pass
        
    def ajouter_arc(self, sommet1, sommet2) :
        '''
        ajoute un arc orienté de sommet1 vers sommet2
        : param sommet1, sommet2 deux sommets existants ou non.
        type int
        : pas de return, EFFET DE BORD sur self
        '''
        self.ajouter_sommet(sommet1)
        self.ajouter_sommet(sommet2)
        if not sommet2 in self.adj[sommet1] :
            self.adj[sommet1].append(sommet2)
        pass
    
    def __repr__(self):
        '''
        renvoie une chaine pour décrir l'objet
        '''
        pass

###################DOCTEST
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)
