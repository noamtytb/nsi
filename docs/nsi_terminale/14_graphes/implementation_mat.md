# Implémentation des graphes à partir de matrice d'adjacence

## Implémentation graphes orientés

1. Récupérer le fichier module_graphe_mat.py sur le commun et placez-le dans votre dossier personnel.

Voici une trace de la classe à implémenter :
![trace_graph_o_mat](Graphe_oriente_mat.png)



### Constructeur

Nous allons représenter les matrices d'adjacences sous forme d'une liste de taille n de liste de booléen de taille n également. 
Pour un graphe de taille 3 n'ayant aucun arc la matrice sera donc la suivante :

```python
[[False,False,False],[False,False,False],[False,False,False]]
```
Les noeuds du graphe sont ainsi représentés avec des indices allant de 0 à $n-1$.

2. Lors de la création d'un graphe on stocke son ordre dans un attribut n. Un attribut mat sera initialisé à une matrice de taille n où tous les éléments sont à faux. À partir des informations ci-dessus, compléter le **constructeur** de la classe graphe_oriente_mat qui prend un paramètre n, l'ordre du graphe.





### Arcs

3. La méthode  **ajouter_arc**  permet comme son nom l'indique d 'ajouter un arc dans le graphe. Cette méthode prend 2 paramètres des entiers $noeud1$ et $noeud2$  représentant 2 noeuds.$noeud1$ est le prédécesseurs de $noeud2$. Cette méthode mets à vrai l'élément $[noeud1] [noeud2]$ de la matrice.  Écrire le corps de cette méthode.

   

4. La méthode **a_arc** vérifie s'il existe un arc entre 2 noeuds. Elle prend 2 paramètres des entiers $noeud1$ et $noeud2$  représentant 2 noeuds. Elle renvoie un booléens.  Écrire le code de cette méthode.





### Successeurs

Dans un graphe orienté on peut convenir que les successeurs d'un noeud sont ces voisins. 

5. La méthode **voisins** renvoie sous forme de liste les successeurs du noeuds qui lui est passé en paramètre. Ainsi, cette méthode prend un paramètre $noeud$, un entier représentant un noeud. Si le noeud n'a pas de successeurs cette méthode renvoie une liste vide. Donner le pseudo code de cette méthode.

   

6. Écrire le corps de la méthode **voisins**.



### Affichage
7. Quelle commande déclenche l'appelle de la méthode **str**?

   > 

   

8. La méthode **str** affiche chaque sommet suivi d'une flèche (->) puis des successeurs du graphe espacés par des espaces. Compléter le code de cette méthode.

   

9. Quelle commande déclenche l'appelle de la méthode **repr** ?

    > 

    

10. La méthode **repr** renvoie une description du graphe de la forme suivante : "Graphe orienté défini par une matrice d'adjacence de taille 3".  Écrire le corps de cette méthode.

    

## Implémentation graphes non orientés

La classe Graphe_non_oriente_mat va hérité de la classe Graphe_oriente_mat.

L'héritage nous permet de ne pas avoir à réécrire les méthodes qui sont les mêmes dans la classe mère que dans la classe fille.

Ainsi en déclarant comme cela la classe "class Graphe_non_oriente_mat(Graphe_oriente_mat) :" on la fait hérité de Graphe_oriente_mat.

La classe Graphe_non_oriente_mat dispose ainsi de toutes les méthodes de la classe Graphe_oriente_mat.

Il reste donc juste à recoder le constructeur et les méthodes dont le comportement n'est pas le même(ajouter_arc et repr).

 ### Constructeur

Il est obligatoire de définir un constructeur dans chaque classe même s'il est le même que la classe mère.

Dans ce cas, il suffit juste d'appeler à l'aide de la méthode super le constructeur de la classe mère.

Ainsi il faut écrire :
```python
super().__init__(n)
```

1. Copier ce code dans le **constructeur** de la classe.

### Arcs

2. La méthode **ajouter_arc **est également a modifier. En effet, lors de l'ajout d'une arète,on ajoute deux 1 dans la matrice. Ici on passera donc 2 valeurs à True.

   Écrire le corps de cette méthode.

### Affichage

3.  Le code de la méthode **repr** change également. En effet, dans la description le graphe est non orienté.

   Écrire le corps de cette méthode.



## Méthodes sur les graphes

codez et documentez les méthodes ci_dessous dans la classe **Graphe_oriente_mat **. Notez que les graphes non orientés hériteront de ces méthodes.Ainsi il faudra peut être les recoder dans la classe **Graphe_non_oriente_mat **.

- `degre(self, noeud)` qui renvoie le degré du noeud, c'est à dire le nombre d'arcs issus de ce noeud.
- `max_degre(self)` qui renvoie le noeud de plus haut degré.
- `nb_arcs(self)` qui renvoie le nombre total d'arcs dans le graphe.
- `supprimer_arc(self, noeud1, noeud2)` qui supprime l'arc orienté entre les deux noeud.

