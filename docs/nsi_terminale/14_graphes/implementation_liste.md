# Implémentation des graphes à partir de liste d'adjacence

## Implémentation graphes orientés

1. Récupérer le fichier module_graphe_liste.py sur le commun et placez-le dans votre dossier personnel.

Voici une trace de la classe à implémenter :
![trace_graph_o_mat](Graphe_oriente_liste.png)



### Constructeur

Nous allons stocker les listes d'adjacence dans un dictionnaire où le nom du nœud est la clé et la valeur est la liste de ses prédécesseurs/successeurs.

Pour cet exercice, la liste retenue est celle des successeurs.



2. Lors de la création d'un graphe on stocke uniquement un dictionnaire vide dans un attribut adj.Le **constructeur**  ne prend pas de paramètre.Écrire le corps de cette méthode.



### Noeuds

3. La méthode **ajouter_noeud** permet d'ajouter un noeud dans la graphe.Cette méthode prend un paramètre $noeud$ qui n'a pas de type défini.SI le noeud ne fait pas partie des clés du dictionnaire cette methode l'ajoute et initialise à liste vide sa valeur.Écrire le corps de cette méthode.

   

4. La méthode **renvoyer_noeuds** renvoie sous forme de liste l'ensemble des noeuds du graphe.Compléter le code de cette méthode.

### Arcs

5. La méthode  **ajouter_arc**  permet comme son nom l'indique d 'ajouter un arc dans le graphe. Cette méthode prend 2 paramètres des entiers $noeud1$ et $noeud2$  représentant 2 noeuds.$noeud1$ est le prédécesseurs de $noeud2$. Cette méthode ajoute  les noeuds au graphe.Puis si $noeud2$ n'est pas dans la liste associé à $noeud1$ elle l'ajoute.  Écrire le corps de cette méthode.
6. La méthode **a_arc** vérifie s'il existe un arc entre 2 noeuds. Elle prend 2 paramètres  $noeud1$ et $noeud2$  représentant 2 noeuds. Elle renvoie un booléens.  Écrire le corps de cette méthode.



### Successeurs

Dans un graphe orienté on peut convenir que les successeurs d'un noeud sont ces voisins.

7. La méthode **voisins** renvoie sous forme de liste les successeurs du noeuds qui lui est passé en paramètre. Ainsi, cette méthode prend un paramètre $noeud$  représentant un noeud. Si le noeud n'a pas de successeurs cette méthode renvoie une liste vide. Écrire le corps de cette méthode.



### Affichage

8. La méthode **str** affiche chaque sommet suivi d'une flèche (->) puis des successeurs du graphe espacés par des espaces.Écrire le corps de cette méthode.

   

9. La méthode **repr** renvoie une description du graphe de la forme suivante : "Graphe orienté de 3 noeuds".  Écrire le corps de cette méthode.




## Implémentation graphes non orientés

Comme pour l'implémentation sous forme de matrice,Graphe_non_oriente_liste va hérité de la classe Graphe_oriente_liste.

Il faut donc de nouveau recoder uniquement le construcuteur ainsi que les méthode ajouter_arc et repr.



### Constructeur

1. A l'aide de la méthode **super** écrire le corps du **constructeur**.



### Arcs

2. La méthode **ajouter_arc** est également à modifier.En effet lors de l'ajout d'une arète, on ajoute également $noeud1$ dans la liste associé à $noeud2$.Compléter le code de cette méthode.



### Affichage

3. Le code de la méthode **repr** change également. En effet, dans la description le graphe est non orienté et il comporte des sommets et non des noeuds.

   Écrire le corps de cette méthode.



## Méthodes sur les graphes

codez et documentez les méthodes ci_dessous dans la classe **Graphe_oriente_liste **. Notez que les graphes non orientés hériteront de ces méthodes.Ainsi il faudra peut être les recoder dans la classe **Graphe_non_oriente_liste **.

- `degre(self, noeud)` qui renvoie le degré du noeud, c'est à dire le nombre d'arcs issus de ce noeud.
- `max_degre(self)` qui renvoie le noeud de plus haut degré.
- `nb_arcs(self)` qui renvoie le nombre total d'arcs dans le graphe.
- `supprimer_arc(self, noeud1, noeud2)` qui supprime l'arc orienté entre les deux noeud.



