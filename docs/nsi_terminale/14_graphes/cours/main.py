from Graphe import *

def dijkstra(G, a):
    sommets = G.get_sommets()
    M = G.matrix()
    fixes = {a}
    reste = set(sommets) - fixes
    poids = dict()
    for s in sommets:
        poids[s] = M[a][s]
    while reste != set():
        m = float('Inf')
        for s in reste:
            if poids[s] < m:
                u = s
                m = poids[s]
        fixes.add(u)
        reste.remove(u)
        for s in reste:
            poids[s] = min(poids[s], poids[u]+M[u][s])
    return poids

g2 = Graphe([1,2,3,4,5,6,7,8], [(1,7,2), (1,4,2), (2,1,1), (3,2,1), (3,7,3), (3,6,2), (4,7,4), (4,8,7), (5,1,5), (5,2,3), (5,3,2), (6,4,1), (6,8,6), (7,4,6)])
print(dijkstra(g2, 5))