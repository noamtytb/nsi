# **Structure de données : Graphes**

## **Mise en situation**

Imaginons un réseau social ayant 6 abonnés (A, B, C, D, E et F) où :

- A est ami avec B, C et D
- B est ami avec A et D
- C est ami avec A, E et D
- D est ami avec tous les autres abonnés
- E est ami avec C, D et F
- F est ami avec E et D

La description de ce réseau social, malgré son faible nombre d'abonnés, est déjà quelque peu rébarbative, alors imaginons cette même description avec un réseau social comportant des millions d'abonnés !

Il existe un moyen plus "visuel" pour représenter ce réseau social : on peut représenter chaque abonné par un cercle (avec le nom de l'abonné situé dans le cercle) et chaque relation "X est ami avec Y" par un segment de droite reliant X et Y ("X est ami avec Y" et "Y est ami avec X" étant représenté par le même segment de droite).

Voici ce que cela donne avec le réseau social décrit ci-dessus :

![graphe d'un réseau](./graph_reseau.PNG)

Ce genre de figure s'appelle un **graphe**. Les graphes sont des objets mathématiques très utilisés, notamment en informatique. Les cercles sont appelés des sommets et les segments de droites qui relient 2 sommets des arêtes.

## **Graphes non Orientés**

Un graphe non orienté *G* est un couple de (*S*, *A*) :
- **S** est un ensemble dont les éléments sont appelés **Sommets**,
- **A** est une suite d'éléments appelés **arêtes** qui sont :
    - soit des singletons {*s*} avec *s* sommet de *G*,
    - soit des paires d'éléments {*x*, *y*} avec *x* et *y* des sommets de *G*.

### **Arêtes, ordre et degré**

Deux sommets *a* et *b* sont **adjacents** si {*a*, *b*} appartient à *A* dans un graphe *G*, les sommets *a* et *b* sont alors des **extrémités** de l'arête *ab*.

On appelle **ordre** d'un graphe *G* le nombre de ses sommets : ordre = card(S).

On appelle **degré** d'un sommet *S* le nombre de sommets qui lui sont adjacents.

**Exemple :**

G = (S, A) avec :
- S = {1, 2, 3, 4, 5}
- A = ({1, 3}, {1, 4}, {1, 5}, {2, 3}, {3, 4}, {3, 5}, {4, 5})

G peut être représenté par :

![exemple orienté](./exemple_graph_oriente.PNG)

G à 5 sommets, l'ordre est donc de 5.

|Sommets|degré|
|:-----:|:---:|
|1      |3    |
|2      |1    |
|3      |4    |
|4      |3    |
|5      |3    |

### **Graphe simple**

Un graphe est dit simple si :
- aucun sommet n'est adjacent à lui-même (il ne contient pas de boucle),
- ne contient pas de double arêtes parallèles.

Le graphe vu précédemment est un graphe simple.

### **Des chemins particuliers**

Lorsque l'on parcours un graphe, nous utilisons des **chaînes**. Une **chaîne** reliant *x* à *y*, notée (*x*, *y*), est définie par une suite finie d'arêtes consécutives, reliant *x* à *y*.

Lorsque l'on parle de **distance** entre 2 sommets *x* et *y*, on parle de la plus petite des longueurs des chaînes reliant *x* à *y*.

Nous avons cinq chaînes particulières :
- Une **chaîne élémentaire** est une chaîne ne passant **pas** deux fois par le même sommet,
- une **chaîne simple** est une chaîne ne passant pas deux fois par la même arête,
- un **cycle** est une chaîne simple avec *x* = *y*,
- une **chaîne hamiltonienne** est une chaîne élémentaire passant par tous les sommets du graphe *G*,
- une **chaîne eulérienne** est une chaîne simple passant par toutes les arêtes du graphe *G*.

### **Graph particuliers**

Un graphe est dit **acyclique** s'il ne contient aucun cycle.

Un graphe est **connexe** si pour chaque paire de sommets, il existe une chaîne qui les relie.

Un graphe est **complet** si ses sommets sont adjacents deux à deux.

Un graphe est dit **hamiltonien** s'il contient un cycle hamiltonien.

Un graphe est dit **eulérien** s'il contient un cycle eulérien.

## **Graphes Orientés**

Un graphe orienté est un graphe pour lequel l'ensemble n'est plus symétrique. Les arêtes sont appelées **arcs**.

Ainsi on distingue l'arc (*x*, *y*) de l'arc (*y*, *x*).

On définit pour chaque sommet du graphe :
- son **degré sortant** égal au nombre d'arcs dont il est la première composante,
- son **degré entrant** égal au nombre d'arcs dont il est la seconde.

**Exemple :**

G = (S, A) avec :
- S = {1, 2, 3, 4, 5, 6}
- A = ((1, 2), (1, 4), (1, 6), (2, 4), (2, 5), (3, 4), (4, 5), (6, 2))

G peut être représenté par :

![exemple non orienté](./exemple_graph_non_oriente.PNG)

G à 6 sommets, l'ordre est donc de 6.

|Sommets|degré entrant|degré sortant|
|:-----:|:-----------:|:-----------:|
|1      |0            |3            |
|2      |2            |2            |
|3      |0            |1            |
|4      |3            |1            |
|5      |2            |0            |
|6      |1            |1            |

## **Représentation informatique des graphes**

Il existe deux façons classiques de représenter un graphe *G* = (*S*, *A*) :
- par un ensemble de **listes d'adjacences**,
- par une **matrice d'adjacences**.

La représentation par **listes d'adjacences** consiste en :
- un tableau *Adj* de *n* listes (*n* étant le nombre de sommets de *G*) : une liste pour chaque sommet de *S*,
- pour tout sommet *x* de *S*, *Adj*\[*x*] est l'ensemble des sommets adjacents à *x*.

La représentation par **matrice d'adjacences** consiste en :
- une numérotation des sommets *e*
- une matrice M = (*a*<sub>*ij*</sub>) où :
    - a<sub>ij</sub> = 1 si les sommets de numéros *i* et *j* sont adjacents,
    - a<sub>ij</sub> = 0 sinon.

### **Représentation avec un dictionnaire**

On peut définir une liste d'adjacences à l'aide d'un dictionnaire qui à chaque sommet associe la liste des sommets qui lui sont adjacents.

Ainsi pour un graphe orienté *G* et pour un graphe non orienté *H* nous pouvons avoir :

```python
G = { 1 : [2, 4, 6], 2 : [4, 5], 3 : [4], 4 : [5], 5 : [], 6 : [2] }

h = { 5 : [7, 8], 6 : [8, 9], 7 : [5, 9], 8 : [6, 9], 9 : [6, 7] }
```

### **Utilisation d'une matrice d'adjacence**

La matrice d'adjacence permet de compter le nombre de chemins reliant deux sommets. Plus précisemment :

Si *M* est la matrice d'adjacence du graphe, alors :

*M*<sub>*ij*</sub><sup>*k*</sup> = *nombre de chemin de longueur k reliant i à j*

**Exemple :**

Sarah, une jeune étudiante en géologie, souhaite partir en voyage en Islande avec des amis. Elle a loué une voiture tout terrain pour pouvoir visiter les lieux remarquables qu'elle a sélectionnés.

Sarah a construit le graphe ci-dessous dont les sommets représentent les lieux ) visiter et les arêtes représentent les routes ou pistes :

![graphe de Sarah](./sarah.PNG)

B : Le lagon bleu
D : Chute d'eau de Dettifoss
G : Geyser de Geysir
H : Rocher Hvitserkur
J : Lagune glacière
L : Massif du Landmannalaugar
M : Lac de Myvatn
R : Capitale Reykja
V : Ville de Vik

On appelle *M* la matrice associée au graphe précédent sachant que les sommets sont placés dans l'ordre alphabétique.

![matrice m](./matrice_m.png)

**Question** - Donner le nombre de chemins de longueur 4 permettant d'aller de *B* à *D*.

Pour calculer le nombre de chemins de longueur 4 permettant d'aller de *B* à *D*, nous avons besoin de caculer la matrice d'adjacence à la puissance 4 : *M*<sup>4</supp>

---

**Multiplication entre 2 matrice de même taille :**

Soit 3 matrices *A*, *B* et *C* de même taille.

![matrices A,B et C](./produit_mat.png)

C<sub>11</sub> = A<sub>11</sub>\*B<sub>11</sub>+A<sub>12</sub>\*B<sub>21</sub>+...+A<sub>1m</sub>\*B<sub>n1</sub>

C<sub>12</sub> = A<sub>11</sub>\*B<sub>11</sub>+A<sub>12</sub>\*B<sub>22</sub>+...+A<sub>1m</sub>\*B<sub>n2</sub>

C<sub>21</sub> = A<sub>21</sub>\*B<sub>11</sub>+A<sub>22</sub>\*B<sub>22</sub>+...+A<sub>2m</sub>\*B<sub>n1</sub>

C<sub>22</sub> = A<sub>21</sub>\*B<sub>12</sub>+A<sub>22</sub>\*B<sub>22</sub>+...+A<sub>2m</sub>\*B<sub>n2</sub>

En général, soit *i* tel que ```1 <= i <= m``` et *j* tel que ```1 <= j <= n```, on a :

C<sub>ij</sub> = A<sub>i1</sub>\*B<sub>1j</sub>+A<sub>i2</sub>\*B<sub>2j</sub>+...+A<sub>im</sub>\*B<sub>nj</sub>

Pour plus d'informations ou pour réaliser des opérations matricielles, se rendre sur : https://matrixcalc.org/fr/

---

Après avoir fait mit la matrice d'adjacence *M* à la puissance 4, voici ce que l'on obtient :

![matrice M à la puissance 4](./matrice_m_pui_4.PNG)

Il faut maintenant regarder à la ligne correspondant au sommet *B* (première ligne) et à la colonne correspondant au sommet *D* (deuxième colonne).

C<sub>12</sub> = 3

Nous avons donc 3 chemins de longueur 4 possibles entre les sommets *B* et *D*.

## **Parcours d'un graphe**

Parcourir un graphe revient à énumérer tous les sommets accessibles par un chemin à partir d'un sommet donné.

### **Parcours en largeur**

L'algorithme de parcours en largeur consiste à utiliser une file (FIFO) pour stocker les sommets à traiter :
- tous les voisins sont traités avant de parcourir le reste du graphe,
- ce type de parcours est idéal pour trouver la plus courte distance entre deux sommets du graphe.

```python
import File as f

def largeur(G, s):
    res = []
    dejaVu = [s]
    aTraiter = f.File()
    aTraiter.enfile(s)
    while not aTraiter.is_empty():
        s = aTraiter.defile()
        res.append(s)
        for v in G[s]:
            if v not in dejaVu:
                aTraiter.enfile(v)
                dejaVu.append(v)
    return res
```

![graphe pour parcours en largeur](./parcours_largeur.png)

```python
>>> largeur(G, 2)
[2, 0, 3, 1, 7, 4, 5, 6, 8]
```

### **Parcours en profondeur**

L'algorithme de parcours en profondeur consiste à utiliser une pile (LIFO) pour stocker les éléments à traiter. On explore chaque chemin jusqu'au bout avant de passer au chemin suivant.

```python
import Pile as p

def longueur(G, s):
    res = []
    dejaVu = [s]
    aTraiter = p.Pile()
    aTraiter.empile(s)
    while not aTraiter.is_empty():
        s = aTraiter.depile()
        res.append(s)
        for v in G[s]:
            if v not in dejaVu:
                aTraiter.empile(v)
                dejaVu.append(v)
    return res
```

![graphe pour un parcours en longueur](./parcours_longueur.png)

```python
>>> longueur(G, 2)
[2, 3, 7, 8, 5, 6, 1, 4, 0]
```

## **Plus court chemin**

Déterminer le plus court chemin entre deux sommets d'un graphe non pondéré n'est pas difficile. Cependant beaucoup de problèmes ajoutent une pondération à chaque arête et définissent le poids d'un chemin comme la somme des poids des arêtes qui le composent.

L'objectif est alors de déterminer le chemin qui minimise le poids entre deux sommets.

### **Algorithme de Dijkstra**

L'algorithme de Dijkstra détermine le chemin de poids minimal entre un sommet *a* et un sommet *b*.

L'algorithme remplit un dictionnaire poids de longueur *n* de sorte qu'à la fin de l'algorithme, poids\[b] soit égal au poids du plus court chemin entre *a* et *b*.

A chaque itération, on fixe le sommet (pas encore fixé) dont le poids du chemin en partant de *s* et ne passant que par des sommets fixés est minimal.

```python
from Graphe import *

def dijkstra(G, a):
    sommets = G.get_sommets()
    M = G.matrix()
    fixes = {a}
    reste = set(sommets) - fixes
    poids = dict()
    for s in sommets:
        poids[s] = M[a][s]
    while reste != set():
        m = float('Inf')
        for s in reste:
            if poids[s] < m:
                u = s
                m = poids[s]
        fixes.add(u)
        reste.remove(u)
        for s in reste:
            poids[s] = min(poids[s], poids[u]+M[u][s])
    return poids
```

**Exemple :**

![graphe pour Dijkstra](./graphe_dijkstra.png)

```python
>>> dijkstra(G, 5)
{1: 4, 2: 3, 3: 2, 4: 5, 5: None, 6: 4, 7: 5, 8: 10}
```

on obtiens donc pour le sommet *5* :

|sommets| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|:-----:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|cout   | 4 | 3 | 2 | 5 | - | 4 | 5 | 10|

le chemin le court entre le sommet *5* et le sommet *8* aura comme poid 10.