#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'Timothée DECOSTER'
__date_creation__ = 'Jan 27 12:52:07 2021'
__doc__ = """
:mod:`graphe` module
:author: {:s} 
:creation date: {:s}
""".format(__author__, __date_creation__)

class Graphe:
    def __init__(self, sommets, aretes):
        self.__sommets = sommets # List<Int>
        self.__sommets.sort()
        self.__aretes = aretes # List<Tupple<Int, Int, Int>>
    
    def get_sommets(self):
        return self.__sommets
    
    def get_aretes_of(self, s):
        res = []
        for el in self.__aretes:
            if el[0] == s:
                res.append(el)
        return res

    def get_aretes(self):
        return self.__aretes

    def voisin(self, s):
        res = []
        for a in self.__aretes:
            if a[0] == s:
                if not a[1] in res:
                    res.append(a[1])
        return res

    def matrix(self):
        res = dict()
        for sommet in self.__sommets:
            res[sommet] = {s : None if s==sommet else float('Inf') for s in self.__sommets}
        for a in self.__aretes:
            res[a[0]][a[1]] = a[2]
        return res