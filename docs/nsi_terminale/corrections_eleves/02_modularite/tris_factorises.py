#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Module contenant des fonctions de tri de tableaux.

Auteur : Ramstein Stéphane
Date de création : 12/09/2022
Contact : stephane.ramstein@ac-lille.fr
'''


## Importation des modules


## Déclaration des constantes


## Déclaration des fonctions

def _recherche_plus_petit(tableau, i):
    '''
    Recherche la position du plus petit élément d'un tableau
    à partir de la position i.
    
    @param_entree : tableau, liste de valeurs
    @param_entree : i, nombre entier
    @param_sortie : position_plus_petit, nombre entier
    '''
    
    n = len(tableau)
    position_plus_petit = i
    for j in range(i, n):
        if tableau[j] < tableau[position_plus_petit]:
            position_plus_petit = j
            
    return position_plus_petit

def _echange_valeurs(tableau, i, position_plus_petit):
    '''
    Echange deux valeurs d'un tableau spécifiées par leur position.
    à partir de la position i.
    
    @param_entree : tableau, liste de valeurs
    @param_entree : i, nombre entier
    @param_entree : position_plus_petit, nombre entier
    @param_sortie : tableau, liste de valeurs
    '''
    temp = tableau[i]
    tableau[i] = tableau[position_plus_petit]
    tableau[position_plus_petit] = temp
    
    return tableau

def tri_selection(tableau):
    '''
    Tri par sélection d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    n = len(tableau)
    for i in range(n):
        position_plus_petit = _recherche_plus_petit(tableau, i)
        tableau = _echange_valeurs(tableau, i, position_plus_petit)

    return tableau

def _decale_valeurs(tableau, element_a_inserer, j):
    '''
    Décale les éléments d'un tableau de la position j à la position 0
    tant que l'élément précédent est plus grand que l'élément à insérer.
    
    @param_entree : tableau, liste de valeurs
    @param_entree : element_a_inserer, nombre entier ou réel
    @param_entree : j, nombre entier
    @param_sortie : j, nombre entier
    '''
    while j > 0 and tableau[j - 1] > element_a_inserer:
        tableau[j] = tableau[j - 1]
        j = j - 1
        
    return j

def tri_insertion(tableau):
    '''
    Tri par insertion d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    
    n = len(tableau)
    for i in range(1, n):
        element_a_inserer = tableau[i]
        j = _decale_valeurs(tableau, element_a_inserer, i)
        tableau[j] = element_a_inserer

    return tableau


## Fonction principale

def main():
    pass

## Programme principal

if __name__ == '__main__':
    main()
