#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import parcours_sequentiel


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau = parcours_sequentiel.creation_tableau(100)
print(tableau)

resultat = parcours_sequentiel.somme(tableau)
print(resultat)

resultat = parcours_sequentiel.moyenne(tableau)
print(resultat)

resultat = parcours_sequentiel.minimum(tableau)
print(resultat)

resultat = parcours_sequentiel.maximum(tableau)
print(resultat)

resultat = parcours_sequentiel.valeurs_extremales(tableau)
print(resultat)

resultat = parcours_sequentiel.recherche_une_occurrence(tableau, 10)
print(resultat)

resultat = parcours_sequentiel.recherche_toutes_occurrences(tableau, 10)
print(resultat)

resultat = parcours_sequentiel.insere_element(tableau, 9999, 10)
print(resultat)