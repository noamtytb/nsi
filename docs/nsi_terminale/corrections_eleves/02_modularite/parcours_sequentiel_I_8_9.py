#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import random


## Déclaration des constantes


## Déclaration des fonctions

def creation_tableau(n):
    '''
    Crée un tableau de n valeurs aléatoires.
    
    @param_entree : n, nombre entier
    @param_sortie : tableau, liste d'entiers
    '''

    tableau = [random.randint(0, 100) for i in range(n)]
    
    return tableau

def somme_nombres(n):
    '''
    Calcule la somme des nombres entre 0 et n.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''

    accumulateur = 0
    for i in range(n + 1):
        accumulateur = accumulateur + i
        
    return accumulateur

def somme(tableau):
    '''
    Calcule la somme des valeurs d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : accumulateur, nombre entier ou réel
    '''
    
    accumulateur = 0
    n = len(tableau)
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
        
    return accumulateur

def moyenne(tableau):
    '''
    Calcule la moyenne d'un tableau de valeurs.
    
    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''
    
    somme = 0
    n = len(tableau)
    for i in range(n):
        somme = somme + tableau[i]
    
    moyenne = somme / n
    
    return moyenne

def minimum(tableau):
    '''
    Recherche le minimum d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_minimum
    '''
    n = len(tableau)
    valeur_minimum = tableau[0]
    for i in range(1, n):
        if tableau[i] < valeur_minimum:
            valeur_minimum = tableau[i]
    
    return valeur_minimum

def maximum(tableau):
    '''
    Recherche le maximum d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_maximum
    '''
    n = len(tableau)
    valeur_maximum = tableau[0]
    for i in range(1, n):
        if tableau[i] > valeur_maximum:
            valeur_maximum = tableau[i]
    
    return valeur_maximum

def valeurs_extremales(tableau):
    '''
    Recherche le minimum et le maximum d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_minimum, nombre entier ou réel
    @param_sortie : valeur_maximum, nombre entier ou réel
    '''
    
    valeur_minimum = minimum(tableau)
    valeur_maximum = maximum(tableau)
    
    return valeur_minimum, valeur_maximum

def recherhe_une_occurrence(tableau, element):
    '''
    Recherche un élément et retourne sa première position.

    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''
    n = len(tableau)
    i = 0
    position = None
    
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
            
    return position

def recherhe_toutes_occurrences(tableau, element):
    '''
    Recherche un élément et retourne toutes ses positions.
    @param_entree : tableau, tableau d'éléments
    @param_entree : element
    @param_sortie : positions, tableau de nombres entiers
    '''
    n = len(tableau)
    positions = []
    for i in range(n):
        if tableau[i] == element:
            positions = positions + [i]

    return positions

def insere_element(liste, valeur_a_inserer, position):
    '''
    Insère un élément à une position dans une liste.

    @param_entree : liste
    @param_entree : valeur_a_inserer
    @param_entree : position
    @param_sortie : liste (modifiée)
    '''
    
    liste.append(None)
    n = len(liste)

    i = n - 1
    while i >= position:
        liste[i] = liste[i - 1]
        i = i - 1
    liste[position] = valeur_a_inserer

    return liste


## Fonction principale

def main():
    
    tableau = creation_tableau(10)
    print(tableau)
    
    resultat = valeurs_extremales(tableau)
    print(resultat)

## Programme principal

if __name__ == '__main__':
    main()