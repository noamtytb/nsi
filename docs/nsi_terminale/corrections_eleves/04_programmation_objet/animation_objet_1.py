#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import tkinter
import time


## Déclaration des classes

class Soleil:
    '''
    Modèle pour créer des objets de type Soleil.
    '''
    
    def __init__(self, x_0, y_0, couleur):
        '''
        Constructeur permettant notamment de définir les attributs.
        
        @param entree : x_0, entier, absisse initiale de l'objet
        @param entree : y_0, entier, ordonnée initiale de l'objet
        @param entree : couleur, chaîne de caractères, couleur de l'objet
        '''
        self.x = x_0
        self.y = y_0
        self.couleur = couleur
        
    def dessiner(self):
        '''
        Dessine un soleil aux positions self.x et self.y.
        '''
        zone_graphique.create_oval(self.x, self.y, self.x + 100, self.y + 100, fill=self.couleur, outline=self.couleur)

class Oiseau:
    '''
    Modèle pour créer des objets de type Oiseau.
    '''
    def __init__(self):
        '''
        Constructeur permettant notamment de définir les attributs.
        '''
        pass
    
    def dessiner(self):
        '''
        Dessine un oiseau aux positions self.x et self.y.
        '''
        pass
    
    def deplacer(self):
        '''
        Deplace un oiseau aux positions self.x et self.y.
        '''
        pass


## Déclaration des fonctions

def sol(y):
    zone_graphique.create_rectangle(0, y, 800 + 100, y + 50, fill='grey', outline='grey')

def oiseau(x, y):
    zone_graphique.create_line(x, y, x + 30, y + 20)
    zone_graphique.create_line(x + 30, y + 20, x + 60, y)

def voiture(x, y):
    zone_graphique.create_rectangle(x + 30, y, x + 120, y + 30, fill='blue', outline='blue')
    zone_graphique.create_rectangle(x, y + 30, x + 150, y + 60, fill='red', outline='red')
    zone_graphique.create_oval(x + 10, y + 50, x + 40, y + 80, fill='yellow', outline='yellow')
    zone_graphique.create_oval(x + 110, y + 50, x + 140, y + 80, fill='yellow', outline='yellow')

def animation():
    running = True
    x = 100
    delta_x = +10
    
    while running == True :
        zone_graphique.delete('all')

        soleil_1.dessiner()
        soleil_2.dessiner()
        sol(550)
        oiseau(x, 200)
        voiture(100, 470)

        zone_graphique.update()
        
        x = x + delta_x
        
        if x < 100 or x > 700:
            delta_x = - delta_x
        
        time.sleep(0.1)


## Programme principal

fenetre = tkinter.Tk()

fenetre.title('Animation')

zone_graphique = tkinter.Canvas(fenetre, width=800, height=600, bg='white')
zone_graphique.pack()

soleil_1 = Soleil(20, 20, 'yellow')
soleil_2 = Soleil(400, 200, 'green')

animation()

fenetre.mainloop()
