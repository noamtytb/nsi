#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import tkinter


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

fenetre = tkinter.Tk()

fenetre.title('Animation')

zone_graphique = tkinter.Canvas(fenetre, width=800, height=600, bg='white')
zone_graphique.pack()

fenetre.mainloop()
