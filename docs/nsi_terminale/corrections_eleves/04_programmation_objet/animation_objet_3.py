#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import tkinter
import time


## Déclaration des classes

class Soleil:
    '''
    Modèle pour créer des objets de type Soleil.
    '''
    
    def __init__(self, x_0, y_0, couleur):
        '''
        Constructeur permettant notamment de définir les attributs.
        
        @param entree : x_0, entier, absisse initiale de l'objet
        @param entree : y_0, entier, ordonnée initiale de l'objet
        @param entree : couleur, chaîne de caractères, couleur de l'objet
        '''
        self.x = x_0
        self.y = y_0
        self.couleur = couleur
        
    def dessiner(self):
        '''
        Dessine un soleil aux positions self.x et self.y.
        '''
        zone_graphique.create_oval(self.x, self.y, self.x + 100, self.y + 100, fill=self.couleur, outline=self.couleur)

class Oiseau:
    '''
    Modèle pour créer des objets de type Oiseau.
    '''
    def __init__(self, x_0, y_0):
        '''
        Constructeur permettant notamment de définir les attributs.
        '''
        self.x = x_0
        self.y = y_0
        self.delta_x = +10
    
    def dessiner(self):
        '''
        Dessine un oiseau aux positions self.x et self.y.
        '''
        zone_graphique.create_line(self.x, self.y, self.x + 30, self.y + 20)
        zone_graphique.create_line(self.x + 30, self.y + 20, self.x + 60, self.y)    

    def deplacer(self):
        '''
        Deplace un oiseau aux positions self.x et self.y.
        '''
        self.x = self.x + self.delta_x
        
        if self.x < 100 or self.x > 700:
            self.delta_x = - self.delta_x

class Sol:
    '''
    Modèle pour créer des objets de type Sol.
    '''
    def __init__(self, y_0, couleur):
        '''
        Constructeur permettant notamment de définir les attributs.
        '''
        self.y = y_0
        self.couleur = couleur

    def dessiner(self):
        '''
        Dessine un sol aux positions self.x et self.y.
        '''
        zone_graphique.create_rectangle(0, self.y, 800 + 100, self.y + 50, fill=self.couleur, outline=self.couleur)

class Voiture:
    '''
    Modèle pour créer des objets de type Voiture.
    '''
    def __init__(self, x_0, y_0):
        '''
        Constructeur permettant notamment de définir les attributs.
        '''
        self.x = x_0
        self.y = y_0
    
    def dessiner(self):
        '''
        Dessine une voiture aux positions self.x et self.y.
        '''
        zone_graphique.create_rectangle(self.x + 30, self.y, self.x + 120, self.y + 30, fill='blue', outline='blue')
        zone_graphique.create_rectangle(self.x, self.y + 30, self.x + 150, self.y + 60, fill='red', outline='red')
        zone_graphique.create_oval(self.x + 10, self.y + 50, self.x + 40, self.y + 80, fill='yellow', outline='yellow')
        zone_graphique.create_oval(self.x + 110, self.y + 50, self.x + 140, self.y + 80, fill='yellow', outline='yellow') 


## Déclaration des fonctions

def animation():
    running = True
    delta_x = +10
    
    while running == True :
        zone_graphique.delete('all')

        soleil_1.dessiner()
        soleil_2.dessiner()
        sol_1.dessiner()
        oiseau_1.dessiner()
        voiture_1.dessiner()
        
        zone_graphique.update()
        
        oiseau_1.deplacer()
        
        time.sleep(0.1)


## Programme principal

fenetre = tkinter.Tk()

fenetre.title('Animation')

zone_graphique = tkinter.Canvas(fenetre, width=800, height=600, bg='white')
zone_graphique.pack()

soleil_1 = Soleil(20, 20, 'yellow')
soleil_2 = Soleil(400, 200, 'green')
sol_1 = Sol(550, 'grey')
oiseau_1 = Oiseau(100, 200)
voiture_1 = Voiture(100, 470)

animation()

fenetre.mainloop()
