#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import tkinter


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

fenetre = tkinter.Tk()

fenetre.title('Animation')

zone_graphique = tkinter.Canvas(fenetre, width=800, height=600, bg='white')
zone_graphique.pack()

zone_graphique.create_rectangle(100, 400, 200, 500)
zone_graphique.create_line(200, 200, 300, 300)
zone_graphique.create_line(200, 300, 300, 200)
zone_graphique.create_line(400, 300, 400, 400)
zone_graphique.create_line(350, 350, 450, 350)

fenetre.mainloop()
