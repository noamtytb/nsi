#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def fonction_1(i):
    i = i + 1
    print(i)
    fonction_1(i)

def fonction_2(i):
    if i < 3:
        i = i + 1
        fonction_2(i)
    else:
        return None # ou pass

def fonction_3(i):
    if i < 3:
        i = i + 1
        fonction_3(i)


## Programme principal

fonction_2(0)