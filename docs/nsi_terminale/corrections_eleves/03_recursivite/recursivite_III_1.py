#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def recherche_dichotomique_recursive(liste_nombres, element_recherche):
    i_milieu = len(liste_nombres) // 2
    if element_recherche == liste_nombres[i_milieu]:
        return i_milieu
    elif element_recherche < liste_nombres[i_milieu]:
        return 0 + recherche_dichotomique_recursive(liste_nombres[:i_milieu], element_recherche)
    elif element_recherche >= liste_nombres[i_milieu]:
        return i_milieu + recherche_dichotomique_recursive(liste_nombres[i_milieu:], element_recherche)


## Programme principal

liste_nombres = list(range(10))
print(liste_nombres)

resultat = recherche_dichotomique_recursive(liste_nombres, 7)
print(resultat)