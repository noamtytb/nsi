#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import turtle


## Déclaration des fonctions

def koch(n, l):
    if n == 0:
        turtle.forward(l)
    else:
        koch(n - 1, l / 3)
        turtle.left(60)
        koch(n - 1, l / 3)
        turtle.right(120)
        koch(n - 1, l / 3)
        turtle.left(60)
        koch(n - 1, l / 3)


## Programme principal

turtle.speed(0)
turtle.hideturtle()
turtle.penup()
turtle.goto(-100, 0)
turtle.pendown()
koch(3, 300)

turtle.done()