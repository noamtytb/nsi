#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def fonction_5(n):
    print('execution de fonction_5(', n, ')')
    if n > 0:
        print(n)
        fonction_5(n - 1)
        print('retour de fonction_5(', n - 1, ') dans fonction_5(', n, ')')


## Programme principal

fonction_5(4)