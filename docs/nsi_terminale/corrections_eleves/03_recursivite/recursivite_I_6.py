#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import time


## Déclaration des fonctions

def fonction_5(n):
    print('execution de fonction_5(', n, ')')
    if n > 0:
        print(n)
        fonction_5(n - 1)
        print('retour de fonction_5(', n - 1, ') dans fonction_5(', n, ')')

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)

def puissance_v1(x, n):
    if n == 0:
        return 1
    else:
        return x * puissance_v1(x, n - 1)

def puissance_v2(x, n):
    if n == 0:
        return 1
    elif n == 1:
        return x
    else:
        return x * puissance_v2(x, n - 1)

def puissance_v3(x, n):
    if n == 0:
        return 1
    elif n % 2 == 0:
        return (puissance_v3(x, n // 2)) ** 2
    else:
        return x * (puissance_v3(x, (n - 1) // 2)) ** 2

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
    
    
## Programme principal

resultat = fibonacci(4)
print(resultat)
