#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import turtle


## Déclaration des fonctions

def branche(n, l, e):
    if n == 0:
        return None  # pass
    else:
        turtle.pensize(e)
        if n == 1:
            turtle.pencolor('red')
        elif n == 2:
            turtle.pencolor('green')
        else:
            turtle.pencolor('brown')
        turtle.forward(l)
        turtle.right(30)
        branche(n - 1, l * 2 / 3, e * 2 / 3)
        turtle.left(60)
        branche(n - 1, l * 2 / 3, e * 2 / 3)
        turtle.right(30)
        turtle.penup()
        turtle.backward(l)
        turtle.pendown()


## Programme principal

turtle.speed(0)
turtle.hideturtle()
turtle.penup()
turtle.left(90)
turtle.goto(0, -100)
turtle.pendown()
branche(7, 150, 10)

turtle.done()