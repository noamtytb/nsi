#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def fonction_5(n):
    if n > 0:
        print(n)
        fonction_5(n - 1)

## Programme principal

fonction_5(4)