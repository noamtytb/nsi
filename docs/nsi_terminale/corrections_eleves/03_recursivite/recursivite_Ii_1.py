#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import turtle


## Déclaration des fonctions

def branche(n, l):
    if n == 0:
        return None  # pass
    else:
        turtle.forward(l)
        turtle.right(30)
        branche(n - 1, l * 2 / 3)
        turtle.left(60)
        branche(n - 1, l * 2 / 3)
        turtle.right(30)
        turtle.backward(l)

## Programme principal

turtle.speed(0)
turtle.penup()
turtle.left(90)
turtle.goto(0, -100)
turtle.pendown()
branche(7, 150)

turtle.done()