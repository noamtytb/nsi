#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules


## Déclaration des constantes


## Déclaration des fonctions

def somme_nombres(n):
    '''
    Calcule la somme des nombres entre 0 et n.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''

    accumulateur = 0
    for i in range(n + 1):
        accumulateur = accumulateur + i
        
    return accumulateur

def somme(tableau):
    '''
    Calcule la somme des valeurs d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : accumulateur, nombre entier ou réel
    '''
    
    accumulateur = 0
    n = len(tableau)
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
        
    return accumulateur

def moyenne(tableau):
    '''
    Calcule la moyenne d'un tableau de valeurs.
    
    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''
    
    somme = 0
    n = len(tableau)
    for i in range(n):
        somme = somme + tableau[i]
    
    moyenne = somme / n
    
    return moyenne

def minimum(tableau):
    '''
    Recherche le minimum d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_minimum
    '''
    n = len(tableau)
    valeur_minimum = tableau[0]
    for i in range(1, n):
        if tableau[i] < valeur_minimum:
            valeur_minimum = tableau[i]
    
    return valeur_minimum

def maximum(tableau):
    '''
    Recherche le maximum d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_maximum
    '''
    n = len(tableau)
    valeur_maximum = tableau[0]
    for i in range(1, n):
        if tableau[i] > valeur_maximum:
            valeur_maximum = tableau[i]
    
    return valeur_maximum

def recherhe_une_occurrence(tableau, element):
    '''
    Recherche un élément et retourne sa première position.

    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''
    n = len(tableau)
    i = 0
    position = None
    
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
            
    return position

def recherhe_toutes_occurrences(tableau, element):
    '''
    Recherche un élément et retourne toutes ses positions.
    @param_entree : tableau, tableau d'éléments
    @param_entree : element
    @param_sortie : positions, tableau de nombres entiers
    '''
    n = len(tableau)
    positions = []
    for i in range(n):
        if tableau[i] == element:
            positions = positions + [i]

    return positions

def tri_selection(tableau):
    '''
    Tri par sélection d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    n = len(tableau)
    for i in range(n):
        position_plus_petit = i
        for j in range(i, n):
            if tableau[j] < tableau[position_plus_petit]:
                position_plus_petit = j
        temp = tableau[i]
        tableau[i] = tableau[position_plus_petit]
        tableau[position_plus_petit] = temp
    
    return tableau

def tri_insertion(tableau):
    '''
    Tri par insertion d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    
    n = len(tableau)
    for i in range(1, n):
        element_a_inserer = tableau[i]
        j = i
        while j > 0 and tableau[j - 1] > element_a_inserer:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = element_a_inserer

    return tableau

def recherche_dichotomique(liste, element_recherche):
    '''
    Recherche un élément et retourne la première position trouvée.

    @param_entree : liste (triée)
    @param_entree : element_recherche
    @param_sortie : position
    '''

    n = len(liste)

    debut_sous_liste = 0
    fin_sous_liste = n - 1
    position = None
    trouve = False
    while trouve == False and debut_sous_liste <= fin_sous_liste:
        i_milieu = (debut_sous_liste + fin_sous_liste) // 2
        if liste[i_milieu] < element_recherche:
            debut_sous_liste = i_milieu + 1
        elif liste[i_milieu] > element_recherche:
            fin_sous_liste = i_milieu - 1
        elif liste[i_milieu] == element_recherche:
            trouve = True
            position = i_milieu

    return position


## Programme principal

resultat = somme_nombres(6)
print(resultat)

resultat = somme([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)

resultat = moyenne([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)

resultat = minimum([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)

resultat = maximum([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)

resultat = recherhe_une_occurrence([5, 7, 12, 1, 0, 8, 13, 20, 19], 1)
print(resultat)

resultat = recherhe_toutes_occurrences([5, 1, 12, 1, 0, 8, 1, 20, 19], 1)
print(resultat)

resultat = tri_selection([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)

resultat = tri_insertion([5, 7, 12, 1, 0, 8, 13, 20, 19])
print(resultat)