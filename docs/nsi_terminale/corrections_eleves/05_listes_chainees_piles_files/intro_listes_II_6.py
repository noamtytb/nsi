#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant
        

## Déclaration des fonctions


## Programme principal

m1 = Maillon(5)
m2 = Maillon(7)

l1 = m1