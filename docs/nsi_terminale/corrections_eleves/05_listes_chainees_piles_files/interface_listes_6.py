#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

class Liste_chainee:
    def __init__(self):
        self.tete = None
        
    def est_vide(self):
        if self.tete == None:
            return True
        else:
            return False

    def ajouter_element_queue(self, valeur):
        if self.est_vide() == True:
            self.tete = Maillon(valeur)
        else:
            maillon = self.tete
            while maillon.suivant != None:
                maillon = maillon.suivant
            
            maillon.suivant = Maillon(valeur)

    def tuple(self):
        if self.est_vide() == True:
            return ()
        else:
            maillon = self.tete
            valeurs = (maillon.valeur,)
            while maillon.suivant != None:
                maillon = maillon.suivant
                valeurs = valeurs + (maillon.valeur,)
            
            return valeurs

    def longueur(self):
        if self.est_vide() == True:
            return 0
        else:
            maillon = self.tete
            compteur = 1
            while maillon.suivant != None:
                maillon = maillon.suivant
                compteur = compteur + 1
            
            return compteur


## Déclaration des fonctions


## Programme principal

l1 = Liste_chainee()
l1.ajouter_element_queue(5)
l1.ajouter_element_queue(7)
l1.ajouter_element_queue(15)
l1.ajouter_element_queue(2)
l1.ajouter_element_queue(8)

print(l1.longueur())