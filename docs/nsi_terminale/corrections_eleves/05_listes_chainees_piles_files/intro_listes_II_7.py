#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant
        

## Déclaration des fonctions


## Programme principal

m1 = Maillon(5)
m2 = Maillon(7)

l1 = m1

print(l1.valeur, l1.suivant)

m1.suivant = m2

print(l1.valeur, l1.suivant.valeur, l1.suivant.suivant)

print(id(l1))
print(id(l1.suivant))
print(id(m2))
