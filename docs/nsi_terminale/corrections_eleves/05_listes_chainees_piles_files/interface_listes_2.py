#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

class Liste_chainee:
    def __init__(self):
        self.tete = None
        
    def est_vide(self):
        if self.tete == None:
            return True
        else:
            return False


## Déclaration des fonctions


## Programme principal

l1 = Liste_chainee()
print(l1.est_vide())