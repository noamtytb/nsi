#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

class Liste_chainee:
    def __init__(self):
        self.tete = None
        
    def est_vide(self):
        if self.tete == None:
            return True
        else:
            return False

    def ajouter_element_queue(self, valeur):
        if self.est_vide() == True:
            self.tete = Maillon(valeur)
        else:
            maillon = self.tete
            while maillon.suivant != None:
                maillon = maillon.suivant
            
            maillon.suivant = Maillon(valeur)

## Déclaration des fonctions


## Programme principal

l1 = Liste_chainee()
print(l1.est_vide())

l1.ajouter_element_queue(5)
print(l1.est_vide())
print(l1.tete.valeur)

l1.ajouter_element_queue(7)
print(l1.tete.valeur, l1.tete.suivant.valeur)