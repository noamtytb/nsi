#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant
        

## Déclaration des fonctions


## Programme principal

l2 = Maillon(1, Maillon(2, Maillon(3, Maillon(4))))
print(l2.valeur, l2.suivant.valeur, l2.suivant.suivant.valeur, l2.suivant.suivant.suivant.valeur, l2.suivant.suivant.suivant.suivant)