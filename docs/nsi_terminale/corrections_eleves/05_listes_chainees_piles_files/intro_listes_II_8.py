#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant
        

## Déclaration des fonctions


## Programme principal

m1 = Maillon(5)
m2 = Maillon(7)
m3 = Maillon(2)
m4 = Maillon(11)

l1 = m1

m1.suivant = m2
m2.suivant = m3
m3.suivant = m4

print(m2.valeur, m2.suivant.valeur)
print(m3.valeur, m3.suivant.valeur)

print(l1.valeur, l1.suivant.valeur, l1.suivant.suivant.valeur, l1.suivant.suivant.suivant.valeur)