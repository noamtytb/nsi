# Sujet 20

## Exercice 1

Le but de cet exercice est le parcours de tableau ainsi que la mise en place de test d'égalité

```python
def xor(a,b):
    res=[]
    for i in range(len(a)):
        if a[i]==b[i]:
            res.append(0)
        else:
            res.append(1)
     return res
```

ligne 1: initialisation de res à une liste vide

ligne 2: parcours des éléments de i

ligne 3: test si les éléments d'indice i sont égaux

ligne 4:  rajout de 0 à res

ligne 5: sinon, on est dans le cas ou les éléments sont égaux

ligne 6: rajout de 1 à res

ligne 7: renvoi de res

## Exercice 2

Le but de cet exercice est de travaiiler les coordonnées à l'intertérieur d'un tableauà 2 dimensions.

```python
class Carre:
    def __init__(self, tableau = [[]]):
        self.ordre = len(tableau)
        self.valeurs = tableau

    def affiche(self):
        '''Affiche un carré'''
        for i in range(self.ordre):
            print(self.valeurs[i])

    def somme_ligne(self, i):
        '''Calcule la somme des valeurs de la ligne i'''
        return sum(self.valeurs[i])

    def somme_col(self, j):
        '''calcule la somme des valeurs de la colonne j'''
        return sum([self.valeurs[i][j] for i in range(self.ordre)])

def est_magique(carre):
    n = carre.ordre
    s = carre.somme_ligne(0)

    #test de la somme de chaque ligne
    for i in range(1, n):
        if carre.somme_ligne(i) != s:
            return False

    #test de la somme de chaque colonne
    for j in range(n):
        if carre.somme_col != s:
            return False

    #test de la somme de chaque diagonale
    if sum([carre.valeurs[k][k] for k in range(n)]) != s:
            return False
    if sum([carre.valeurs[k][n-1-k] for k in range(n)]) != s:
            return False

    return True   

c2=Carre([[1,1],[1,1]])
est_magique(c2)
c3=Carre([[2,9,4],[7,5,3],[6,1,8]])
est_magique(c3)
c4=Carre([[4,5,16,9],[14,7,2,11],[3,10,15,6],[13,12,8,1])
est_magique(c4)
```

ligne 1: initilisation de n à l'odre du carré

ligne 2: initialisation de s à la somme de la première ligne

ligne 3: parcours des lignes 1 à n-1 du carré

ligne 4: test si la somme de la ligne en cours n'est pas égale à s

ligne 5: renvoi de faux

ligne 6: parcours des colonne du carré

ligne 7: test si la somme de la colonne en cours n'est pas égale à s

ligne 8: renvoi de faux

ligne 9:test si la somme de la diagonale qui part de en haut à gauche n'est pas égale ) s

ligne 10 : renvoi de faux

ligne 11:test si la somme de la diagonale qui part de en haut à droite n'est pas égale ) s

ligne 12 : renvoi de faux

ligne 13: renvoi de vrai, si l'ont arrive ici, toutes les lignes,les colonnes et les diagonales sont égales
