# Sujet 34

## exerice 1

Le but de cet exercice est le parcours de chaine de caractère ainsi que l'usage de la méthode count

```python
def occurence_max(chaine):
    max=0
    elem=None
    for car in chaine: 
        if chaine.count(car)>max:
            elem=car
            max=chaine.count(car)
    return elem
```

ligne 1 initialisation de la variable max à 0, elle stockera le nombre d'occurence de l'élément elem

ligne 2: initialisation de elem à none, elem sera l'élément le plus fréquent dans la chaine.

ligne 3: parcours des caractères de la chaine

ligne 4 : test si le nombre d'occurence de car dans la chaine est supérieur au max

ligne5: elem devient car

ligne 6: max devient le nombre d'occurence de car dans la chaine 

ligne 7: renvoi de l'élément 

## exercice 2

Le but de cet exercice est l'utilisation de liste de liste ainsi que la manipulation d'indice.

```python
def nbLig(image):
    '''renvoie le nombre de lignes de l'image'''
    return len(image)

def nbCol(image):
    '''renvoie la largeur de l'image'''
    return len(image[0])

def negatif(image):
    '''renvoie le negatif de l'image sous la forme 
       d'une liste de listes'''
    L = [[0 for k in range(nbCol(image))] for i in range(nbLig(image))] 
# on cree une image de 0 aux memes dimensions que le parametre image 
    for i in range(len(image)):
        for j in range(len(image[0])):
            L[i][j] = 255-image[i][j]
    return L

def binaire(image, seuil):
    '''renvoie une image binarisee de l'image sous la forme 
       d'une liste de listes contenant des 0 si la valeur 
       du pixel est strictement inferieure au seuil 
       et 1 sinon'''
    L = [[0 for k in range(nbCol(image))] for i in range(nbLig(image))] # on cree une image de 0 aux memes dimensions que le parametre image 
    for i in range(len(image)):
        for j in range(len(image[0])):
            if image[i][j] < seuil :
                L[i][j] = 0
            else:
                L[i][j] = 1
    return L

```

### fonction nbLig:

renvoie la taille de la liste image car la hauteur de l'image est le nombre de sous liste



### fonction nbCol:

renvoie la taille de la premiere sous liste de image car la largeur de l'image est  la longueur d'une sous liste 



### fonction negatif: 

ligne 1: affectation à L  d'une image de 0 aux memes dimensions que le parametre image 

ligne 2: parcours des lignes de l'image

ligne 3: parcours des pixels de l'image

ligne 4: affectation à Lij le négatif de celle-ci soit 255- imageij

ligne 5 renvoi de l'image négative donc L



## fonction seuil

ligne 1: affectation à L  d'une image de 0 aux memes dimensions que le parametre image 

ligne 2: parcours des lignes de l'image

ligne 3: parcours des pixels de l'image

ligne 4: test si la valeur du pixel à la place image ij est inérieur au seuil 

ligne 5: affectation de 0 à Lij

ligne 6: sinon, on est donc dans le cas ou la valeur du pixel à la place image ij est supérieur au seuil 

ligne 7: affection de 1 à lij

ligne 8: renvoi de l'image L



