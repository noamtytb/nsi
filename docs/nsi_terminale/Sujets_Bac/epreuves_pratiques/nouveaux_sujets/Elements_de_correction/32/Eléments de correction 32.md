# Eléments de correction 24

## Exercice 1

```python
def recherche(elt,tab):
    res=-1
    for i in range(len(tab)):
        if tab[i]==elt:
            res=i
    return res
```

Le but de cet exercice est de rechercher la dernière occurrence d'un élément dans une liste si elle est présente et -1 sinon. Cet exercice fait travailler le parcours de liste et la sélection d'élément à l'aide d'indice dans une liste.

ligne 1: initialisation  du résultat à -1, le résultat ne changera pas si l'élément elt n'est pas présent dans la liste 

ligne 2: parcours des éléments de la liste par leur indice

ligne 3: si l'élément à l'indice i est égale à l'élément elt

ligne 4: le résultat est égal à l'indice de l'élément dans la liste égale à elt

ligne 5: renvoie du résultat  



## Exercice 2

```python
class AdresseIP:

    def __init__(self, adresse):
        self.adresse = adresse
   
    def liste_octet(self):
        """renvoie une liste de nombres entiers,
           la liste des octets de l'adresse IP"""
        return [int(i) for i in self.adresse.split(".")] 
        
    def est_reservee(self):
        """renvoie True si l'adresse IP est une adresse
           réservée, False sinon"""
        return self.adresse=="192.168.0.0" or "192.168.0.255"
             
    def adresse_suivante(self):
        """renvoie un objet de AdresseIP avec l'adresse 
           IP qui suit l’adresse self
           si elle existe et False sinon"""
        if self.liste_octet[3] < 254:
            octet_nouveau = self.liste_octet[3] + 1
            return AdresseIP('192.168.0.' +  octet_nouveau)
        else:
            return False
adresse1=AdresseIP('192.168.0.1')
adresse2=AdresseIP('192.168.0.2')
adresse3=AdresseIP('192.168.0.3')
```

Ici, il y a une erreur dans le sujet dans est_reservee le return est le résultat d'un test d'égalité

### le constructeur

on donne à la variable adresse la valeur du paramètre adresse qui est une chaine de caractère

### la méthode est_reservee

renvoie le résultat du test entre self.adresse et 192.168.0.0 et 192.168.0.255 qui sont des adresses réservées

### la méthode adresse_suivante

ligne 1: on est dans le cas ou le dernier octet est inférieur à 254 ainsi il existe encore une adresse ip non réservé après

Il faut donc récupérer le dernier octet à l'aide de la méthode liste_octet et prendre le 4ème élément donc d'indice 3

ligne 2: le nouvel octet est égal à l'ancien octet plus 1

ligne 3: renvoie de l'adresse avec le nouveau dernier octet

ligne 5: sinon on est donc dans le cas ou le dernier octet est> ou égale à 254

ligne 6: renvoyer faux