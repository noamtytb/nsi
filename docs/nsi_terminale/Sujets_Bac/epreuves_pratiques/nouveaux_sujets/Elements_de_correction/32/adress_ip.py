def recherche(elt,tab):
    res=-1
    for i in range(len(tab)):
        if tab[i]==elt:
            res=i
    return res


class AdresseIP:

    def __init__(self, adresse):
        self.adresse = adresse
   
    def liste_octet(self):
        """renvoie une liste de nombres entiers,
           la liste des octets de l'adresse IP"""
        return [int(i) for i in self.adresse.split(".")] 
        
    def est_reservee(self):
        """renvoie True si l'adresse IP est une adresse
           réservée, False sinon"""
        return "192.168.0.0" or "192.168.0.255"
             
    def adresse_suivante(self):
        """renvoie un objet de AdresseIP avec l'adresse 
           IP qui suit l’adresse self
           si elle existe et False sinon"""
        if self.adresse[10:13] < 254:
            octet_nouveau = self.adresse[10:13] + 1
            return AdresseIP('192.168.0.' +  octet_nouveau)
        else:
            return False
