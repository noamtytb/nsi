# Sujet 4

## Exercice 1

Le but de cet exercice est le parcours de liste à l'aide de ses indices et la manipulation des tuples dans une liste

```python
def recherche(tab):
    res=[]
    for i in range(len(tab)-1):
        if tab[i+1] == tab[i]+1:
            res.append((tab[i],tab[i+1]))
    return res

# Programme principal

print(recherche([1, 4, 3, 5]))
print(recherche([1, 4, 5, 3]))
print(recherche([7, 1, 2, 5, 3, 4]))
print(recherche([5, 1, 2, 3, 8, -5, -4, 7]))

```

ligne 1: initialisation à liste vide du résultat

ligne 2 : parcours des indice de la liste sauf le dernier

ligne 3 : test si l'élément d'indice i+1 est égal à l'élément d'indice i +1

ligne 4: ajouteàa res le couple (tab[i],tab[i+1])

ligne 5: renvoi de res

## Exercice 2

LE but de cet exercice est l'utilisation d'indice dans un tableau à 2 dimensions

```python
def propager(M, i, j, val):
    if M[i][j]== 0:
        return M

    M[i][j]=val

    # l'élément en haut fait partie de la composante
    if ((i-1) >= 0 and M[i-1][j] == 1):
        propager(M, i-1, j, val)

    # l'élément en bas fait partie de la composante
    if ((i+1) < len(M) and M[i+1][j] == 1):
        propager(M, i+1, j, val)

    # l'élément à gauche fait partie de la composante
    if ((j-1) >= 0 and M[i][j-1] == 1):
        propager(M, i, j-1, val)

    # l'élément à droite fait partie de la composante
    if ((j+1) < len(M) and M[i][j+1] == 1):
        propager(M, i, j+1, val)

```

ligne 1: test si mij est égale à 0

ligne 2: renvoi de l'image tel quel

ligne 3: affectation de val à mij

ligne 4: test si l'élément en haut(mi-1J) fait partie de la composante

ligne 5: fonction propager sur l'élément du haut

ligne 6: test si l'élément en bas(mi+1J) fait partie de la composante

ligne 7: fonction propager sur l'élément du bas

ligne 8: test si l'élément à gauche(miJ-1) fait partie de la composante

ligne 9: fonction propager sur l'élément à gauche

ligne 10: test si l'élément à droite(miJ+1) fait partie de la composante

ligne 11: fonction propager sur l'élément à droite
