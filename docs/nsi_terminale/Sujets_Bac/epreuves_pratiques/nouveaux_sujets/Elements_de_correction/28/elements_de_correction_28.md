# Elements de correction Sujet 16

## Exercice 1

Le but de cet exercice est de fournir une fonction qui renvoit la moyenne des valeurs d'un tableau.

```python
def moyenne(liste):
    
    somme = 0
    taille = len(liste)
    
    for nombre in liste:
        somme = somme + nombre
    
    return somme / taille
```

Ligne 3 : On initialise une variable somme à 0

Ligne 4 : On récupère la taille de la liste passée en paramètres

Ligne 6 et 7 : Pour chaque nombre de la liste, on ajoute le nombre dans notre variable somme

Ligne 9 : On renvoie la somme divisé par la taille, c'est notre moyenne

## Exercice 2

Le but de cet exercice est de compléter le code fourni afin d'avoir une fonction convertissant les nombre en écriture décimale en nombre écrit en binaire sous la forme d'une chaine de caractère

```python
def dec_to_bin(a)
    bin_a = str(a%2)
    a = a//2
    while a != 0:
        bin_a = str(a%2) + bin_a
        a = a//2
    return bin_a
```

On a ici 4 espaces à remplir

Ligne 2 : str(a%2). a%2 est le bit le plus à droite du nombre, le str est pour convertir le 0 ou le 1 en caractère

Ligne 4 : while a != 0 : On continue la boucle tant que a n'est pas nul, car cela signifie qu'il reste des choses à mettre dans la chaine bin_a

Ligne 5 : str(a%2) pour avoir le prochain bit du nombre en chaine de caractère

Ligne 6 : a//2 -> Afin de diminuer a pour que la boucle s'arrête un jour