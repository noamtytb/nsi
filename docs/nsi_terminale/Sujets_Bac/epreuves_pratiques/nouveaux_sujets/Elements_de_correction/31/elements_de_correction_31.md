# Elements de correction Sujet 22

## Exercice 1

Le but de cet exercice est de fournir une fonction qui renvoit le nombre d'occurence d'un élément a dans un tableau t

```python
def recherche(a, t):
    compteur = 0
    for element in t:
        if a == element:
            compteur = compteur + 1
    return compteur
```

Ligne 2 : On initialise une variable compteur à 0

Ligne 3 : On va parcourir tout les éléments du tableau t

Ligne 4 et 5 : Si on trouve a dans le tableau t, alors on augmente notre compteur de 1

Ligne 6 : On renvoie notre compteur, c'est le nombre de a dans t

## Exercice 2

Le but de cet exercice est de compléter le code afin d'obtenir les pièces que l'on doit rendre.
C'est un algorithme glouton.

```python
def rendu_monnaie_centimes(s_due, s_versee):
    pieces = [1, 2, 5, 10, 20, 50, 100, 200]
    rendu = []
    a_rendre = s_versee - s_due
    i = len(pieces)-1
    while a_rendre > 0:
        if pieces[i] <= a_rendre :
            rendu.append(pieces[i])
            a_rendre = a_rendre - pieces[i]
        else:
            i = i-1
    return rendu
```

Ligne 3 : On initialise rendu à une liste vide, c'est cette liste (remplie) que l'on renverra à la fin de la fonction

Ligne 4 : la somme à rendre est égale à la somme versée moins la somme dûe

Ligne 6 : Tant que a_rendre n'est pas nul, c'est que l'on peut ajouter des pieces.

Ligne 8 : On ajoute à nos pièces à rendre la piece d'indice i (qui est la plus grande possible)

Ligne 9 : On soustrait la pièce à a_rendre

Ligne 11 : La pièce i est trop grande, on doit en prendre une plus petite désormais.