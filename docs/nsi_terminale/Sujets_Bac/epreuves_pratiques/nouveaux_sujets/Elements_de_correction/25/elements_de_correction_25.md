# Éléments de correction Sujet 25

## Exercice 1 

Le but de cet exercice est de fournir une fonction qui renvoie la liste des animaux ayant pour numéro d'enclos celui mis en paramètre

```python
def selection_enclos(table_animaux, num_enclos):
    res = []
    for animal in table_animaux:
        if animal['enclos'] == num_enclos:
            res.append(animal)
    return res
```

ligne 1 : initialisation de res à une liste vide
ligne 2 : pour chaque animal de table_animaux
ligne 3 et 4 : si l'animal a pour numéro d'enclos num_enclos, alors on l'ajoute à notre liste res
ligne 5 : on renvoie la liste res
		

## Exercice 2

Le but de cet exercice est de compléter la fonction trouver_intrus qui met en oeuvre un algorithme de recherche récursive.

```python
def trouver_intrus(tab, g, d):
    '''
    Renvoie la valeur de l'intrus situé entre les indices g et d 
    dans la liste tab où 
    tab vérifie les conditions de l'exercice,
    g et d sont des multiples de 3.
    '''
    if g == d:
        return tab[g]
    else:
        nombre_de_triplets = (d - g)// 3
        indice = g + 3 * (nombre_de_triplets // 2)
        if tab[indice] == tab[indice + 2]:
            return trouver_intrus(tab, indice+3, d)
        else:
            return trouver_intrus(tab, g, indice)
```
ligne 2 : si notre borne gauche (g) et notre borne droite (d) sont identiques, alors on a l'intrus à l'indice g (ou d, vu que c'est le même)

ligne 4 : on divise par trois car les triplets c'est des groupes de 3 éléments

ligne 6 : si l'élément d'indice indice et celui deux indice plus loin sont identiques 
ligne 7 : alors l'intrus est dans la partie droite du tableau, on cherche l'intrus dedans
ligne 8 et 9 : sinon l'intrus est dans la partie gauche, on cherche l'intrus dedans
