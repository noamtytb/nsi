# Sujet 24

## Exercice 1

Le but de cet exercice est d'utiliser les fonctions et méthodes python sur les listes afin de trouver l'élément maximum d'une liste ainsi que le parcours de liste.

Pour cet exercice 2 méthodes de résolution sont possibles:

- la première en utilisant les fonctions python
- la seconde en pacourant les éléments de la liste 

```python
def maxliste(tab):
    return max(tab)
```

ligne 1 : renvoi de l'element max du tableau 

```python
def maxliste(tab):
    max=tab[0]
    for elem in tab:
        if elem>max:
            max=elem
     return max
```

ligne 1: affectation a max du premier élément du tableau 

ligne 2 parcours des éléments de tab

ligne 3 : test si l'élément est supérieur au max

ligne 4 : affectation à max de l'élément 

ligne 5 : renvoi du max

## Exercice 2

Le but de cet exercice est d'utiliser une pile pour vérifier le parenthésage d'un texte.

```python
class Pile:
    """ Classe définissant une pile """
    def __init__(self, valeurs=[]):
        self.valeurs = valeurs

    def est_vide(self):
        """Renvoie True si la pile est vide, False sinon"""
        return self.valeurs == []

    def empiler(self, c):
        """Place l’élément c au sommet de la pile"""
        self.valeurs.append(c)

    def depiler(self):
        """Supprime l’élément placé au sommet de la pile, à condition qu’elle soit non vide"""
        if self.est_vide() == False:
            self.valeurs.pop()


def parenthesage (ch):
    """Renvoie True si la chaîne ch est bien parenthésée et False sinon"""
    p = Pile()
    for c in ch:
        if c == '(':
            p.empiler(c)
        elif c == ')':
            if p.est_vide():
                return False
            else:
                p.depiler()
    return p.est_vide()

assert parenthesage("((()())(()))") == True
assert parenthesage("())(()") == False
assert parenthesage("(())(()") == False
```

ligne 1: affectation à p d'une pile vide

ligne 2: parcours des caractères de la chaine 

ligne 3: test si le caractère est une parenthése ouvrante

ligne 4: le caractère est empiler sur p

ligne 5: test si le caractère est une parenthèse fermante

ligne 5: test si la pile est vide

ligne 6: renvoi de false, on est ici dans le cas où un parenhtèse fermante est placé sans parenthése ouvrante qui lui correspond au préalable

ligne 7: sinon , la pile n'est pas vide

ligne 8: on depile p 

ligne 9: renvoi de True si la pile est vide et ainsi la chaine est bien parenthésé et False sinon 