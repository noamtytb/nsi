# Elements de correction

## Exercice 1

Cette exercice a pour but l'utilisation soit des boucles, soit de la récursivité afin d'effectuer une multiplication uniquement avec des additions et des soustractions.

La subtilité est ici dans le choix de l'addition ou de la soustraction en fonction du signe des nombres que l'on multiplie

### 1ère méthode (avec des boucles)

``` python
def multiplication(n1, n2):
    res = 0
    if n2 > 0:
        for i in range(n2):
            res += n1
    else:
        for i in range(n2, 0):
            res -= n1
    return res
```

ligne 2 : initialisation d'un variable "res" (pour résultat) à 0

ligne 3 : test de la positivité de n2

ligne 4 et 5 : boucle, on ajoute (car n2 est positif) n1 à "res" n2 fois

ligne 6 : si n2 n'est pas positif, on entre dans ce cas

ligne 7 et 8 : boucle, on soustrait n1 (car n2 est négatif) à "res" -n2 fois

ligne 9 : renvoie de res, le résultat de la multiplication

### 2 ème méthode

``` python
def multiplication(n1, n2):
    if n2 == 0:
        return 0
    elif n2 < 0:
        return -n1 + multiplication(n1, n2+1)
    else:
        return n1 + multiplication(n1, n2-1)
```

ligne 2 et 3 : Cas initial, si n2 est nul on renvoie 0

ligne 4 : si n2 est négatif, on rentre dans la condition

ligne 5 : renvoie -n1 (car n2 négatif) plus le résultat de la multiplication de n1 et n2+1

ligne 6 : si n2 est positif, on rentre dans la condition

ligne 7 : renvoie n1 (car n2 positif) plus le résultat de la multiplication de n1 et n2-1

Ici, peut importe les nombres pris au départ, on retombera toujours sur le cas initial grâce à la récursivité. Si n2 est positif, on va diminuer n2 pour le rapprocher de 0. Si n2 est négatif, on va augmenter n2 pour le rapprocher de 0.

###

## Exercice 2

```python
def chercher(T,n,i,j):
    if i < 0 or j >= len(T) :
        print("Erreur")
        return None
    if i > j :
        return None
    m = (i+j) // 2
    if T[m] < n :
        return chercher(T, n, m+1 , j)
    elif T[m] > n :
        return chercher(T, n, i , m-1)
    else :
        return m 
```

Le but de cet exercice est de compléter un algorithme de recherche dichotomique

Ligne 2 : Si j est un indice trop grand pour notre liste initiale, alors on affiche une erreur.

Ligne 7 : m est le milieu de i et j, donc on divise par 2 la somme de i et j

Ligne 8 et 9: Si T[m] est plus petit que ce que l'on recherche, ici n, alors on va effectuer la recherche sur la tranche m+1 : j

Ligne 10 et 11 : Si T[m] est plus grand que n, alors on va effectuer la recherche sur la tranche i : m-1

Ligne 12 et 13 : Sinon cela veut dire que T[m] == n, donc on renvoie m

