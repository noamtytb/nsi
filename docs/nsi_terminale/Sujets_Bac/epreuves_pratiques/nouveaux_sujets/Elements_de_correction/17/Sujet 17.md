# Sujet 17

## Exercice 1

le but de cet exercice est de parcourir les éléments d'une chaine de caractère.

Si la phrase de termine par un ! ou un ? alors le nombre de mots est égale au nombre d'espaces. 

Sinon le nombre de mots est égale au nombre d'espace +1.

```python
def nombre_de_mots(phrase):
    res=0
    for carac in phrase:
        if carac== ' ':
            res+=1
     if phrase[-1]=='.':
        res+=1
     return res
```

ligne 1: initialisation du résultat à 0

ligne 2 parcours des caractère de la phrase

ligne 3: test si le caractère est un espace

ligne 4: ajout de 1 à res

ligne 5: test si le caractère de fin de la phrase est un point 

ligne 6: ajout de 1 à res

ligne 7: renvoie du résultat.



## Exercice 2

```python
class Noeud:
    '''
    Classe implémentant un noeud d'arbre binaire 
    disposant de 3 attributs :
    - valeur : la valeur de l'étiquette,
    - gauche : le sous-arbre gauche.
    - droit : le sous-arbre droit.
    '''
    def __init__(self, v, g, d):
        self.valeur = v
        self.gauche = g
        self.droite = d
        
class ABR:
    '''
    Classe implémentant une structure 
    d'arbre binaire de recherche.
    '''
    
    def __init__(self):
        '''Crée un arbre binaire de recherche vide'''
        self.racine = None
        
    def est_vide(self):
        '''Renvoie True si l'ABR est vide et False sinon.'''
        return self.racine is None
    
    def parcours(self, tab = []):
        '''
	  Renvoie la liste tab complétée avec tous les 
        éléments de 
        l'ABR triés par ordre croissant.
        '''
        if self.est_vide():
            return tab
        else:
            self.racine.gauche.parcours(tab)
            tab.append(self.racine)
            self.racine.droite.parcours(tab)
            return tab
        
    def insere(self, element):
        '''Insère un élément dans l'arbre binaire de recherche.'''
        if self.est_vide():
            self.racine = Noeud(element, ABR(), ABR())
        else:
            if element < self.racine.valeur:
                self.racine.gauche.insere(element)
            else : 
                self.racine.droite.insere(element)
    
    def recherche(self, element):
        '''
        Renvoie True si element est présent dans l'arbre 
        binaire et False sinon.
	 '''
        if self.est_vide():
            return False
        else:
            if element < self.racine.valeur:
                return self.racine.gauche.recherche(element)
            elif element > self.racine.valeur:
                return self.racine.droite.recherche(element)
            else: 
                return True
```

la méthode parcours: 

on est dans le cas d'un arbre binaire de recherche aisi le parcours est infixe. On parours le sous arbre gauche puis la racin est enfin le sous arbre droit.

ligne 1: si l'abre est vide

ligne 2: on renvoie le tableau tel quel

ligne 3: parcours du sous arbre gauche 

ligne 4: ajout de la racine à tab

ligne 5: parcours du sous arbre droit 



la méthode recherche:

ligne 1: test si l'arbre est vide

ligne 2: renvoie de faux 

ligne 3: sinon, si l'arbre n'est pas vide

ligne 4: test si l'élément est inférieur à la racine 

ligne 5: recherche de l'élément dans le sous arbre gauche 

ligne 6: test si l'élément est supérieur à la racine 

ligne 7: recherche de l'élément dans le sous arbre droit 

ligne 8: sinon , on est donc dans la cas ou l'élément est égale à la racine 

ligne 9: renvoi de vrai