# Eléments de correction sujet 37 



## Exercice 1

Le but de cet exercice est le parcours de liste ainsi que l'accés aux éléments d'une liste grace à leur indice.

```python
def verifie(tab):
  for i in range (len(tab)-1):
    if tab[i]>tab[i+1]:
      return False
  return True
```

ligne 1: parcours des n-1 premiers éléments

ligne 2: test si l'élément d'indice i est plus grand que l'élément d'indice i+1

ligne 3: renvoyer False

ligne 4: renvoyer True, on est dans le cas où la liste est triée 

## Exercice 2

Le but de cet exercice est de manipuler les dictionnaires.

```python
def depouille(urne):
    resultat = {}
    for bulletin in urne:
        if bulletin in resultat :
          resultat[bulletin] = resultat[bulletin] + 1
        else:
          resultat[bulletin] = 1
    return resultat

def vainqueur(election):
    vainqueur = ''
    nmax = 0
    for candidat in election:
      if nmax < election[candidat]:
        nmax = election[candidat]
        vainqueur = candidat
    liste_finale = [nom for nom in election if election[nom] ==nmax]
    return liste_finale

```

### Fonction depouille

ligne 1: creation d'un dictionnaire vide

ligne 2: parcours des bulletins de l'urne

ligne 3: test si l'artiste est deja dans la liste de resultat 

ligne 4:  incrementation de 1 de la valeur dans le dictionnaire

ligne 5: sinon, on est donc dans le cas ou l'artiste n'est pas encore dans le resultat

ligne 6: initialisation à 1 de la valeur de l'artiste dans le resultat

ligne 7: renvoi du dictionnaire resultat



### Fonction vainqueur 

ligne 1 :  intialisation de la variable vainqueur à une chaine de caractère vide

ligne 2 : initialisation de nmax à 0, nmax est le nombre max de vote

ligne 3: parcours des candidats dans le dictionnaire

ligne 4: test si nmax est inférieur à la valeur correspondante au candidat dans le dictionnaire 

ligne 5: affectation à nmax de la valeur correspondante au candidat dans le dictionnaire 

ligne 6:  affectation à vainqueur du candidat 

ligne 7:  création de la liste_finale et ajout de tous les candidats qui ont la valeur nmax 

ligne 8: renvoi du tableau liste_finale