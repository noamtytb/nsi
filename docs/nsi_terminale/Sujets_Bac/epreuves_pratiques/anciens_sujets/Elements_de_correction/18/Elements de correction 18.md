# Elements de correction

## Exercice 1

Le but de cet exercice est de renvoyer l'indice de la première occurrence d'un élément dans une liste en la parcourant.

```python
def recherche(elt,tab):
    res=-1
    i=0
    while res==-1 and i<len(tab):
        if tab[i]==elt:
            res=i
        i+=1
    return res
```

ligne 1: initialisation d'une variable res à -1, cette variable sera le résultat ainsi si l'élément n'est pas présent dans la liste alors la variable restera à -1 et s'il y est elle sera égale à la valeur de l'indice.

ligne 2: initialisation de la variable i à 0, ici i représente l'indice de parcours de la liste

ligne 3: parcours de la liste tant que res est égal à -1 (c'est-à-dire tant que elt n'a pas était détecté dans tab)  et que i est inférieur à la longueur du tableau(si i est égal à la longueur du tableau alors on a déjà parcouru tout le tableau)

ligne 4: si l'élément d'indice i est égal à elt

ligne 5: on affecte i à res

ligne 6: incrémentation de i

ligne 7: renvoie du résultat



## Exercice 2

le but de cet exercice est d'insérer un élément dans une liste déjà triée. Pour cela, il faut utiliser le parcours de liste et les index.

```python
def insere(a, tab):
    l = list(tab) #l contient les mêmes éléments que tab
    l.append(a)
    i = len(l)-2
    while a < l[i] and i>=0 : 
      l[i+1] = l[i]
      l[i] = a
      i = i-1
    return l
```

ligne 1 : copie de tab dans l

ligne 2: ajout de a à la fin de la liste

ligne 3:  initialisation de i à la longueur de la liste l - 2, ici i sera l'indice de parcours de la liste, le dernier élément de la liste étant à on commence donc à la longueur de la liste -2

ligne 4: tant que a est inférieur à l'élément d'indice i et que i est positif ou nul.

ligne 5: on décale l'élément d'indice i à la place i+1

ligne 6: a est placé à l'indice i

ligne 7: décrémentation de 1 de i

ligne 8 renvoie de l 

