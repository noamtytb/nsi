def moyenne(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=0
        for elem in tab:
            somme+=elem
        moyenne=somme/len(tab)
        return moyenne
    
def moyenne_python(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=sum(tab)
        moyenne=somme/len(tab)
        return moyenne
    

def tri(tab):
    #i est le premier indice de la zone non triee, j le dernier indice. 
    #Au debut, la zone non triee est le tableau entier.
    i= 0
    j= len(tab)-1
    while i != j :
        if tab[i]== 0:
            i= i+1
        else :
            valeur = tab[j]
            tab[j] = 1
            tab[i]=valeur
            j= j-1
    return tab