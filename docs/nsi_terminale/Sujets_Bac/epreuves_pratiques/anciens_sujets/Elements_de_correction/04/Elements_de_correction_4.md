# Eléments de correction

## Exercice 1

Le but de cet exercice est de calculer la moyenne des éléments d'une liste la première façon est itérative(en parcourant les éléments de la liste), la seconde est en utilisant les fonctions python

```python
def moyenne(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=0
        for elem in tab:
            somme+=elem
        moyenne=somme/len(tab)
        return moyenne
```

ligne 1: si la liste est vide

ligne 2: renvoie de la chaine de caractère erreur

ligne 3: sinon donc si la liste n'est pas vide

ligne 4: initialisation de la variable somme à 0, cette variable contiendra la somme des éléments de la liste

ligne 5: parcours des éléments de la liste

ligne 6: ajout de l'élément à somme

ligne 7: stockage dans moyenne la moyenne

ligne 8: renvoie de la moyenne

```python
def moyenne_python(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=sum(tab)
        moyenne=somme/len(tab)
        return moyenne
```

Dans cette version nous allons utiliser la fonction sum du module list.

ligne 1: si la liste est vide

ligne 2: renvoie de la chaine de caractère erreur

ligne 3: sinon donc si la liste n'est pas vide

ligne 4: stockage dans somme la somme des éléments du tableau

ligne 5: stockage dans moyenne la moyenne

ligne 6: renvoie de la moyenne

## Exercice 2

Le but de cet exercice est de trouver une valeur dans un tableau trié à l'aide de la recherche dichotomique.

```python
def dichotomie(tab, x):
    """
        tab : tableau trié dans l’ordre croissant
        x : nombre entier
        La fonction renvoie True si tab contient x et False sinon
    """
    # cas du tableau vide
    if len(tab)==0:
        return False,1

    # cas où x n'est pas compris entre les valeurs extrêmes
    if (x < tab[0]) or (x > tab[-1])):
        return False,2
    
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = (fin-debut)//2+debut
        if x == tab[m]:
            return True
        if x > tab[m]:
            debut = m + 1
        else:
            fin = m			
    return (False,3)

```

Dans cet exercice 4 cas sont différenciés:

- Le premier est de renvoyer (False,1) si la liste est vide pour cela les lignes 1 et 2 sont utilisées.

  ligne 1: test que le tableau est vide

  ligne 2: renvoie (False,1)

- Le second cas est de renvoyer (False,2) si x n'est pas compris entre les valeurs extrêmes du tableau  pour cela les lignes 3 et 4 sont utilisées.

  ligne 3: test si x est plus petit que la plus petite valeur du tableau ou plus grand que la plus grande valeur du tableau 

  ligne 4: renvoie de (False,2)

- les 2 derniers cas sont de renvoyer True si x est dans le tableau et (False,3) sinon.

  Pour cela l'algorithme de recherche dichotomique est utilisé.

  ligne 5: initialisation de la variable début qui sera l'indice de début de la tranche de recherche

  ligne 6: initialisation de la variable fin qui sera l'indice de fin de la tranche de recherche

  ligne 7: tant que la tranche a une taille supérieure à 0

  ligne 8: m prend la valeur de l'indice du milieu de la tranche de recherche 

  ligne 9: si x est égale à l'élément du milieu de la tranche 

  ligne 10: renvoyer True

  ligne 11: si x est supérieur à l'élément du milieu de la tranche

  ligne 12: l'indice du début de la nouvelle tranche est donc m+1

  ligne 13: sinon, on est donc dans le cas ou x est inférieur à l'élément du milieu de la tranche

  ligne 14: l'indice de fin de la nouvelle tranche de recherche est  m

  ligne 15: on est ici dans le cas ou l'on a parcouru l'ensemble de la liste et ou la tranche de recherche a une taille de 0, on renvoie ainsi (False,3)