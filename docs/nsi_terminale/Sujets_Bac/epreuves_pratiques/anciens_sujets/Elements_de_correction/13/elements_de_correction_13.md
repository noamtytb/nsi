# Element de correction Sujet 13

## Exercice 1

Dans cet exercice vous devez redonner le code en python du tri par sélection

```python
def tri_selection(tab):
    n = len(tab)
    for i in range(n):
        j_du_plus_petit = i
        for j in range(i, n):
            if tab[j] < tab[j_du_plus_petit]:
                j_du_plus_petit = j
        temp = tab[i]
        tab[i] = tab[j_du_plus_petit]
        tab[j_du_plus_petit] = temp
    return tab
```

A chaque itération de la première boucle for, on va chercher l'indice du plus petit élément grâce à la seconde boucle for, puis mettre le plus petit élément à sa place (et celui à la place "actuelle" à l'ancienne place du plus petit élément)

## Exercice 2

Le but de cet exercice est de compléter le code afin d'avoir un jeu du plus ou moins opérationnel.

```python
from random import randint

def plus_ou_moins():
    nb_mystere = randint(1,99)
    nb_test = int(input("Proposez un nombre entre 1 et 99 : "))
    compteur = 1

    while nb_mystere != nb_test and compteur < 10 :
        compteur = compteur + 1
        if nb_mystere > nb_test:
            nb_test = int(input("Trop petit ! Testez encore : "))
        else:
            nb_test = int(input("Trop grand ! Testez encore : "))

    if nb_mystere == nb_test:
        print ("Bravo ! Le nombre était ",nb_mystere)
        print("Nombre d'essais: ",compteur)
    else:
        print ("Perdu ! Le nombre était ",nb_mystere)
```

Le nombre mystere étant entre 1 et 99, on doit mettre 99 dans le randint afin d'avoir des nombres uniquement compris entre 1 et 99 inclus

Le compteur compte le nombre d'essai. On le démarre donc à 1 (ligne 4), on lui ajoute 1 à chaque fois (ligne 7), et on l'affiche quand on affiche le nombre d'essais

A la fin on affiche le nombre mystere que l'on ai gagné ou perdu

La boucle while continue si deux conditions sont respectées. La première, que le nombre mystere ne soit pas trouvé. La seconde, que le joueur en soit à moins de 10 essais.