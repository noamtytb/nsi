# Elements de correction



## Exercice 1

Le but de cet exercice est de programmer un algorithme de recherche dichotomique.

```python
def recherche(tab,n):
    if (n < tab[0]) or (n > tab[-1])):
        return -1
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = (fin-debut)//2+debut
        if n == tab[m]:
            return m
        if n > tab[m]:
            debut = m + 1
        else:
            fin = m			
    return -1
```

ligne 1: test si n est plus petit que la plus petite valeur du tableau ou plus grand que la plus grande valeur du tableau 

ligne 2: renvoie de -1

ligne 3: initialisation de la variable début qui sera l'indice de début de la tranche de recherche

ligne 4: initialisation de la variable fin qui sera l'indice de fin de la tranche de recherche

ligne 5: tant que la tranche a une taille supérieure à 0

ligne 6: m prend la valeur de l'indice du milieu de la tranche de recherche 

ligne 7: si n est égale à l'élément du milieu de la tranche 

ligne 8: renvoyer m

ligne 9: si n est supérieur à l'élément du milieu de la tranche

ligne 10: l'indice du début de la nouvelle tranche est donc m+1

ligne 11: sinon, on est donc dans le cas ou n est inférieur à l'élément du milieu de la tranche

ligne 12: l'indice de fin de la nouvelle tranche de recherche est  m

ligne 13: on est ici dans le cas ou l'on a parcouru l'ensemble de la liste et ou la tranche de recherche a une taille de 0, on renvoie ainsi -1

## Exercice 2

Le but de cet exercice est de programmer le codage césar. Ici il n'est pas préciser que si la lettre n'est pas dans l'alphabet elle n'est pas changée dans le nouveau message.

```python
ALPHABET='ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def position_alphabet(lettre):
    return ALPHABET.find(lettre)

def cesar(message, decalage):
    resultat = ''
    for lettre in message :
        if lettre in ALPHABET :
            indice = ( position_alaphabet(lettre)+decalage )%26
            resultat = resultat + ALPHABET[indice]
        else:
            resultat = resultat+lettre
    return resultat

```

Seule la fonction césar sera commentée ici.

ligne 1: initialisation du résultat à une chaine de caractère vide

ligne 2 :parcours des lettres dans le message

ligne 3: si la lettre est dans l'alphabet

ligne 4 l'indice de la lettre  codée est égal à sa position dans l'alphabet + le décalage modulo 26 

ligne 5: ajout de la lettre codé au résultat

ligne 6: sinon, on est donc dans le cas ou la lettre n'est pas dans l'alphabet

ligne 7: ajout de la lettre au résultat

ligne 8: renvoie du résultat

