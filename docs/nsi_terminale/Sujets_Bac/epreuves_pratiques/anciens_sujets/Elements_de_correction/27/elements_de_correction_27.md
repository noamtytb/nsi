# Elements de correction Sujet 27

## Exercice 1

Le but de cet exercice est de fournir une fonction qui renvoit la moyenne des valeurs d'un tableau.

```python
def moyenne(liste):
    
    somme = 0
    taille = len(liste)
    
    for nombre in liste:
        somme = somme + nombre
    
    return somme / taille
```

Ligne 3 : On initialise une variable somme à 0

Ligne 4 : On récupère la taille de la liste passée en paramètres

Ligne 6 et 7 : Pour chaque nombre de la liste, on ajoute le nombre dans notre variable somme

Ligne 9 : On renvoie la somme divisé par la taille, c'est notre moyenne

## Exercice 2

Le but de cet exercice est de compléter le code afin d'obtenir le même résultats.

Une liste pour dessiner un coeur et la fonction "affichage" sont déjà donné, vous n'avez pas à y toucher.

```python
def zoomListe(liste_depart,k):
    '''renvoie une liste contenant k fois chaque 
       élément de liste_depart'''
    liste_zoom = []
    for elt in liste_depart :
        for i in range(k):
            liste_zoom.append(elt)
    return liste_zoom

def zoomDessin(grille,k):
    '''renvoie une grille où les lignes sont zoomées k fois 
       ET répétées k fois'''
    grille_zoom=[]
    for elt in grille:
        liste_zoom = zoomListe(elt, k)
        for i in range(k):
            grille_zoom.append(liste_zoom)
    return grille_zoom
```

Dans zoomListe :
- on initialise liste_zoom avec une liste vide
- Pour chaque element (elt) de la liste_depart, on fera k fois ce qui suit :
- on ajoute (append) l'élement (elt) à la liste_zoom

Dans zoomDessin :
- on définit liste_zoom comme étant le résultats de zoomListe(elt, k)
- on ajoute (append) liste_zoom à grille_zoom
