# Eléments de correction
## Exercice 1

Le but de cet exercice est de compter le nombre d'occurrence d'un caractère dans un mot en utilisant un compteur et une boucle.

```python
def rechcerche(caractere,mot):
    cpt=0
    for car in mot:
        if car==caractere:
            cpt+=1
     return cpt
```

ligne 1: initialisation du compteur à 0

ligne 2 : parcours des caractères du mot

ligne 3: test d'égalité entre le caractère en cours du mot et le caractère de référence

ligne 4: incrémentation de 1 du compteur

ligne 5: renvoie du compteur

## Exercice 2

Le but de cet exercice est d'utiliser un algorithme glouton pour rendre la monnaie avec le moins de pièce possible.

Ici, la stratégie gloutonne est de rendre la maximun de pièce de plus grande valeurs en premier puis ensuite de la seconde plus grande valeur et ainsi de suite.

Dans cet exercice il est demandé de travailler de manière recursive. La fonction prend 3 paramètres :

- arendre: qui est la somme qui reste à rendre
- solution est la liste contenant les pièces déja rendu
- i est l'indice dans la liste pièce

```python
def rendu_glouton(arendre, solution=[], i=0):
    if arendre == 0:
        return solution
    p = pieces[i]
    if p <= arendre :
        return rendu_glouton(arendre - p, solution.append(p),i)
    else :
        return rendu_glouton(arendre, solution, i+1)
```

ligne 1:   test que la somme à rendre est nulle

ligne 2: renvoyer la solution

ligne 3: on est ici dans le cas ou il reste de l'argent à rendre p prend la valeur de la pièce à l'indice i dans le système monétaire.

ligne 4: si p est plus petit que la somme à rendre

ligne 5: appel recursif en enlevant p de la somme à rendre et en ajoutant p dans la solution, ici i ne change pas, car il est possible que la somme à rendre soit encore plus grande que la pièce en cours.

ligne 6: on est ici dans le cas ou p est plus grand que la somme à rendre on doit donc prendre une pièce plus petite

ligne 7: appel recursif en incrémentant i de 1 et donc en prenant la pièce suivant la plus petite.