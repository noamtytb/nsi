# Elements de correction

## Exercice 1

Cette exercice a pour but l'utilisation soit des boucles, soit de la récursivité afin d'effectuer une multiplication uniquement avec des additions et des soustractions.

La subtilité est ici dans le choix de l'addition ou de la soustraction en fonction du signe des nombres que l'on multiplie

### 1ère méthode (avec des boucles)

``` python
def multiplication(n1, n2):
    res = 0
    if n2 > 0:
        for i in range(n2):
            res += n1
    else:
        for i in range(n2, 0):
            res -= n1
    return res
```

ligne 2 : initialisation d'un variable "res" (pour résultat) à 0

ligne 3 : test de la positivité de n2

ligne 4 et 5 : boucle, on ajoute (car n2 est positif) n1 à "res" n2 fois

ligne 6 : si n2 n'est pas positif, on entre dans ce cas

ligne 7 et 8 : boucle, on soustrait n1 (car n2 est négatif) à "res" -n2 fois

ligne 9 : renvoie de res, le résultat de la multiplication

### 2 ème méthode

``` python
def multiplication(n1, n2):
    if n2 == 0:
        return 0
    elif n2 < 0:
        return -n1 + multiplication(n1, n2+1)
    else:
        return n1 + multiplication(n1, n2-1)
```

ligne 2 et 3 : Cas initial, si n2 est nul on renvoie 0

ligne 4 : si n2 est négatif, on rentre dans la condition

ligne 5 : renvoie -n1 (car n2 négatif) plus le résultat de la multiplication de n1 et n2+1

ligne 6 : si n2 est positif, on rentre dans la condition

ligne 7 : renvoie n1 (car n2 positif) plus le résultat de la multiplication de n1 et n2-1

Ici, peut importe les nombres pris au départ, on retombera toujours sur le cas initial grâce à la récursivité. Si n2 est positif, on va diminuer n2 pour le rapprocher de 0. Si n2 est négatif, on va augmenter n2 pour le rapprocher de 0.

###

## Exercice 2

```python
def dichotomie(tab, x):
    """
    tab : tableau d'entiers trié dans l'ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = (debut+fin)//2
        if x == tab[m]:
            return True
        if x > tab[m]:
            debut = m + 1
        else:
            fin = m - 1
    return False
```

Le but de cet exercice est de redonner l'algorithme de recherche dichotomique, le principe étant celui du "diviser pour mieux régner".

Les lignes à compléter sont les lignes 4, 6, 10 et 11.

Ligne 4 : m est défini comme étant le milieu de début et fin. 

Ligne 6 : si x == tab[m], cela veut dire que x est trouvé. Donc on renvoie True

Ligne 10 : si le x est plus petit que tab[m], cela veut dire que la fin est trop "à droite", donc on va la rapprocher. m et ses successeurs ayant déjà été examiné, on fixe fin à m-1

Ligne 11 : si on atteint cette ligne de code, c'est que x n'est pas trouvé, donc on renvoie False

