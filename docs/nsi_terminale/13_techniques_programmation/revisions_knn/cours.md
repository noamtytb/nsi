# L'algorithme de knn 

L’algorithme des knn(nearest neighbors,k plus proches voisins) est l’un des algorithmes utilisés dans le domaine de l’intelligence artificielle.  Il intervient dans de nombreux domaines de l’apprentissage automatique.

Il est par exemple utilisé par des entreprises d'Internet  comme Amazon, netflix, Spotify ou iTunes afin de prévoir si vous seriez  ou non intéressés par un produit donné en utilisant vos données et en les  comparant à celles des clients ayant acheté ce produit particulier.        

Son principe peut être résumé par cette phrase : *Dis-moi qui sont tes amis et je te dirai qui tu es.*        

## Exemple

Dans cette exemple, nous considérons un jeu de données constitué de la façon suivante :

- les données sont réparties suivant deux types : le type 1 et le type 2,
- les données n'ont que deux caractéristiques : caractéristique 1 et caractéristique 2,

Imaginez la situation suivante dans un jeu :

- Vous avez deux types de personnages : les fantassins (type 1 : "fantassin") et les chevaliers (type 2 : "chevalier").
- Vous avez deux types de caractéristiques : la force  (caractéristique 1 : nombre entre 0 et 20) et le courage  (Caractéristique 2 : nombre entre 0 et 20 ).                
- Vous avez une collection de personnages dont vous connaissez les caractéristiques et le type.

 Vous introduisez un nouveau personnage dont vous ne  connaissez pas le type. Vous possédez les caractéristiques de ce nouveau personnage.  Le but de l'algorithme KNN  est de déterminer le type de ce nouveau personnage.

Les données sont stockées dans un fichier csv téléchargeable : [ fichier csv à télécharger](http://www.monlyceenumerique.fr/nsi_premiere/algo_a/dl/personnages.csv)

Voici un aperçu des données :                ![img](http://www.monlyceenumerique.fr/nsi_premiere/algo_a/img/a4_knn_exemple3.png)            

 Voici une représentation de ces données : 

![img](http://www.monlyceenumerique.fr/nsi_premiere/algo_a/img/a4_knn_exemple1.png)

Nous introduisons une nouvelle donnée (appelée cible dans notre exemple) avec ses deux caractéristiques : une force de 12 et un  courage de 12,5 . Le but de l'algorithme KNN est de déterminer le type de cette nouvelle donnée.

1. Dans un premier, il faut fixer le nombre de voisins. Nous allons choisir k=7.

 Voici une nouvelle représentation avec la cible et la recherche des 7 voisins les plus proches proches, ceux qui se  trouvent dans le cercle bleu : 

![img](http://www.monlyceenumerique.fr/nsi_premiere/algo_a/img/a4_knn_exemple2.png)

On remarque ici que pour k=7 notre cible est entourée de 5 fantassins et de 2 chevaliers  ainsi selon l'algorithme des knn elle serait du type fantassin.                

 On considère désormais la valeur k =13

1. Voici une nouvelle représentation avec la cible et la recherche des 13 voisins les plus proches proches, ceux qui se trouvent dans le cercle bleu :                        ![img](http://www.monlyceenumerique.fr/nsi_premiere/algo_a/img/a4_knn_exercice1_q2.png)                        On remarque ici que pour k=13 notre cible est entourée de 6 fantassins et de 7 chevaliers  ainsi selon l'algorithme des knn elle serait du type chevalier.

## Choix du K

Les valeurs k=7 et k=13 sont ici des choix arbitraires. On remarque que le choix du k influe sur le résultat.

La valeur de k doit néanmoins être choisie  judicieusement : trop faible,  la qualité de la prédiction diminue ; trop  grande, la qualité de la prédiction diminue aussi. Il suffit d'imaginer qu'il existe une classe prédominante en nombre.  Avec une grande valeur de k, cette classe remporterait la prédiction à chaque fois.   

## L'algorithme

Pour prédire la classe d’un nouvel élément, il faut des données :            

- Un échantillon de données ;                    
-  Un nouvel élément dont on connaît les caractéristiques et dont on veut prédire le type ;                    
-  La valeur de k : le nombre de voisins étudiés.                    


 Une fois ces données modélisées, nous pouvons formaliser l'algorithme de la façon suivante :            

1. Trouver, dans l’échantillon, les k plus proches voisins de l'élément à déterminer.                    
2. Parmi ces proches_voisins, trouver la classification majoritaire.                    
3.  Renvoyer la classification_majoritaire comme type cherché de l'élément.                    

Ce qui nous donne l'algorithme naïf suivant :

- `Données` : 

- - une `table` de données de taille n ;
  - une donnée cible ;
  - un entier plus petit que n ;
  - une règle permettant de calculer la `distance` entre deux données.

- `Algorithme permettant d'obtenir les k plus proches voisins`  : 

1. Trier les données de la table selon la distance croissante avec la donnée cible.
2. Créer la liste des k premières données de la table triée.
3. Renvoyer la classe majoritaire dans cette liste 



## La distance

La notion de distance est un élément central de cet algorithme. Voici quelques distances possibles :        

**La distance Euclidienne (dans un repère orthonormé)**

Soit deux données donnée1 et donnée2 de coordonnées respectives (x1,y1) et (x2,y2)

distance(donnée1,donnée2)=$√(x1−x2)²+(y1−y2)²$



**La distance de Manhattan**

Soit deux données donnée1(x1,y1) et donnée2(x2,y2).

 distance(donnée1,donnée2)=$|x1−x2|+|y1−y2|$



**La distance de Tchebychev**

Soit deux données donnée1(x1,y1) et donnée2(x2,y2).

 distance(donnée1,donnée2)=$max(|x1−x2|,|y1−y2| )$.

