# TP KNN

une entreprise de prêt à porter souhaite prédire la taille d'un T-Shirt à partir des données taille et poids du client.

Pour cela des essayages ont été faits sur une échantillon de 18 personnes. Il est donné dans le fichier [csv](./echantillon_taille_t_shirt.csv) .



Voici une representation sous forme de graphique des données de ce fichier.

![graph](https://gitlab.com/stephane_ramstein/nsi/-/raw/master/docs/nsi_premiere/10_algorithmique/assets/TP_algo_04_algotithmes_intelligents-f7604a7f.png)

1. Un client a une taille de 161 cm pour un poids de 61 kg. Ajouter son point représentatif sur le graphique. Quelle taille de T-Shirt lui semble adaptée ?
2. Pour chaque objet de la collection, calculer la distance euclidienne entre le nouveau point et tous les autre points.

3. Classer les données par distance croissante et recenser les votes pour K allant de 1 à 5.
4. En déduire la taille de T-shirt du client.

On va maintenant faire de même en python.

5. Récupérer les fichiers sur le git.

6. ouvrir le fichier vetements_eleve.py. Dans ce fichier le traitements des données du fichier csv a déja été fait. Ainsi dans liste_descripteur vous trouverez les descripteur taille poids et objets. liste_objets est une liste de liste, chacune des listes représente un objet. Ces listes sont composées de 2 entiers(la taille, le poids) ainsi que d'une chaine de caractères (la taille du t shirt).

7. Ecrire une fonction `distance_euclidienne(objet_1, objet_2)` qui calcule la distance euclidienne entre un objet `objet_1` et un autre objet `objet_2`. Les objets sont les listes contenues dans liste_objets.

8. Ecrire une fonction `calcule_distances(objet, liste_objets)` qui calcule toutes les distances euclidiennes entre un objet `objet` et tous les objets d'une liste d'objets `liste_objets`. La fonction retourne la liste d'objets à laquelle la distance a été ajoutée à la suite des valeurs des descripteurs de chacun des objets. L'objet test est `[161, 61]` pour 161 cm et 61 kg.

9. Ecrire une fonction `compte_voisins(objet, liste_objets)` qui retourne le nombre de voisins de type M et de type L d'un objet pour chaque valeur de k. Le retour se fera sous la forme d'une liste de listes de deux éléments.

   l.sort(key= lambda x: x[3]) perlet de trier la liste l en fonction du 4 eme éléments.

   Le résultat attendu pour l'objet test est :

   ```
   [[1, 0], [2, 0], [3, 0], [4, 0], [4, 1], [5, 1], [6, 1], [6, 2], [6, 3], [6, 4], [7, 4], [7, 5], [7, 6], [7, 7], [7, 8], [7, 9], [7, 10], [7, 11]]
   ```

   `[1, 0]` signifie que pour `k = 1`, on compte 1 M et 0 L.

   `[2, 0]` signifie que pour `k = 2`, on compte 2 M et 0 L.

   ...

   `[7, 11]` signifie que pour `k = 18`, on compte 7 M et 11 L.

10. Ecrire une fonction `plus_proche_voisins(objet, liste_objets, k)` qui retourne la prédiction M ou L pour la valeur spécifiée de `k`.

11. Que prédit la méthode pour l'objet test avec `k = 5` ?
