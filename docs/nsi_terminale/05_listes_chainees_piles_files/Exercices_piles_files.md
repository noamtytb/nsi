# Exercices sur les piles et les files



## Exercice 1

Soit une pile P composée des éléments suivants  : 12, 14, 8, 7, 19 et 22 (le sommet de la pile est 22) .

- Que renvoie depiler(P) ? De quels éléments est maintenant composée P ?

- On effectue empiler(P,42) .De quels éléments est maintenant composée P ?

- Que renvoie sommet(P)? La pile est elle modifiée?

- Après avoir appliqué depiler(P) une fois, Quelle est la taille de la pile ?

- Si on applique depiler(P) 6 fois de suite,  que renvoie est_vide(P)?		

  ​	

## Exercice 2

Soit une file F composée des éléments suivants  : 12, 14, 8, 7, 19 et 22 (le premier élément rentré dans la file est 22 ; le dernier élément  rentré dans la file est 12).  

- On effectue enfiler(F,42) .De quels éléments est maintenant composée F ?

- Que renvoie defiler(F) ? De quels éléments est maintenant composée F ?

-  Que renvoie tete(F)? La file est elle modifiée?

- si on applique defiler(F) 6 fois de suite, que renvoie est_vide(F)?	

  ​	

## Exercice 3

On forme une  pile en empilant successivement toutes les lettres du mot informatique en commençant donc par le I.

1. On dépile une lettre. Quelle est cette lettre ?
2. On dépile maintenant 5 lettres. Quel élément se trouve alors au sommet de la pile ?



## Exercice 4

Même exercice que le 1 avec une file.

On forme une  file en enfilant successivement  toutes les lettres du mot informatique en commençant donc par le I..

1. On défile une lettre. Quelle est cette lettre ?
2. On défile maintenant 5 lettres. Quel élément se trouve alors en tête de la file ?





## Exercice 5

On se donne trois piles *P1*, *P2* et *P3*.

- La pile *P1* contient des nombres entiers positifs empilés, par exemple 4 9 2 5 3 et 8, 8 étant au dessus de la pile.
- Les piles *P2* et *P3* sont initialement vides.

En manipulant les cartes(dépilant, empilant ...) trouvez les manipulations à faire pour réaliser les exercices ci-dessous. Ecrivez, ensuite, en pseudo-code, l'algorithme à réaliser dans chaque cas.Puis coder le en python.

1. On veut inverser l'ordre des éléments de la pile *P1*.
2. On veut déplacer un entier sur deux dans *P2* ou *P3*.
3. On veut déplacer les entiers de *P1* de façon à avoir les entiers pairs dans *P2* et les impairs dans *P3*.



## Exercice 6

On dispose uniquement de la structure de donnée de Pile et on souhaite créer, en utilisant deux piles P1 et P2, une nouvelle structure de File. On décide que la P1 contiendra les valeurs de la file. Le tête de P1 sera la tête de la file.

1. Comment vérifier que la file est vide ou pas ?
2. Comment défiler l'élément de tête de la file ?
3. Comment faire alors pour enfiler un élément en queue de file (donc en queue de P1) ?




