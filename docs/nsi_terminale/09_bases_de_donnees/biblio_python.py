import sqlite3
fichierDonnees = 'biblio.db'

#La méthode connect prend en paramètre le chemin vers la base de /
#données et retourne un objet de type Connection. Si la base /
#n'existe pas, cette instruction crée une base vide. 
conn = sqlite3.connect(fichierDonnees)

#Une fois la connexion établie, on peut créer un curseur (un objet de type Cursor) par la méthode cursor()
#de Connection.
cur=conn.cursor()

#Un curseur dispose d'une méthode execute() qui prend en paramètre
#une chaîne de caractères représentant une requête SQL
cur.execute("SELECT * FROM usager ;")
personnes = cur.fetchall() # récupère toutes les données dans la tables usager en n-upplets 
print(personnes)

# le nom et le prénom de ces personnes :
for personne in personnes:
    print(personne[0], personne[1])

#Il ne faut pas oublier de fermer la connexion à la fin du programme
conn.close()