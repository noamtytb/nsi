## Importation des modules

import turtle


## Déclaration des fonctions

def carre(taille):
    for i in range(4):
        turtle.forward(taille)
        turtle.left(90)

## Programme principal

carre(50)

turtle.forward(200)

carre(100)

turtle.done()