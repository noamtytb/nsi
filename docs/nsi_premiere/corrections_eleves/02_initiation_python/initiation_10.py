## Importation des modules


## Déclaration des fonctions


## Programme principal

# Les nombres entiers (integer)
# a = 52
# print(a)
# print(type(a))

# Les nombres réels (float)
# a = 32.
# print(a)
# print(type(a))

# Les nombres complexes (complex)
# a = 7 + 5j
# print(a)
# print(type(a))

# Les chaînes de caractères (string)
# a = 'Les misérables'
# print(a)
# print(type(a))

# La concaténation
# b = 'est une oeuvre de'
# c = a + b
# print(c)

# La nouvelle chaine peut remplacer l'ancienne
# d = 'Victor Hugo'
# c = c + d
# print(c)

# Indexation
# a = 'Les misérables'
# print(a)
# print(a[-1])

# Slicing
a = 'Les misérables'
print(a[7:])
