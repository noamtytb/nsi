## Importation des modules


## Déclaration des fonctions


## Programme principal

# Les n-uplets (tuples)
a = ('amarillo', 'verde', 'azul', 'rojo')
# print(a, 'est du type', type(a))
# 
# print(len(a))

# S'utilisent comme les chaînes de caractères
a = a + ('morado',)
# print(a)
# print(a[0], a[1], a[2:])
# 
# # Dimensions multiples
# b = ((1, 2, 3, 4), (5, 6), (7, 8, (10, 12)))
# print(b[0], b[0][3], b[2][2], b[2][2][1])
# 
# Les n-uplets sont non modifiables
# (non mutables)

# a = (1, 2, 3)
# 
# print(a)
# 
# a = (1, 7, 3)
# 
# print(a)

# a[2] = 'rosa' # génère une erreur

# Les tableaux (lists)
# a =  ['amarillo', 'verde', 'azul', 'rojo']
# print(a, 'est du type', type(a))



# S'utilisent comme les n-uplets
# avec en plus la méthode .append()
# a = a + ['morado']
# print(a)
# a.append('blanco')
# print(a)
# 
# Les tableaux sont modifiables
# (mutables)
# a[2] = 'rosa'
# print(a)

# Les dictionnaires (dictionaries)
# ensembles de paires clé:valeur
a = {'français': 15, 'maths': 12, 'Hist-Géo': 10.5}
print(a, 'est du type', type(a))

# L'indexation se fait avec les clés
print(a['Hist-Géo'])

# Les dictionnaires sont modifiables
# (mutables)
a['maths'] = 13
print(a)

# Ajout d'une paire clé:valeur
a['SVT'] = 19.5
print(a)
