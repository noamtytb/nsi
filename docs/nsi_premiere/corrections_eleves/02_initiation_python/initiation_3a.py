## Importation des modules


## Déclaration des fonctions


## Programme principal

print(3 < 5)
print(5 < 3)
print()

print(3 == 3)
print(3 == 5)
print(3 != 5)
print(3 != 3)
print()

print(3 == 3)
print(3 == 3.)
print(3 is 3.)

if True:
    print('yo')
else:
    print('payo')
    
if False:
    print('yo')
else:
    print('payo')