## Importation des modules

# Déclaration des fonctions

def trouve_langue_couleur(couleur):
    couleurs_francais = ['jaune', 'vert', 'bleu', 'rouge']
    couleurs_espagnol = ['amarillo', 'verde', 'azul', 'rojo']
    
    if couleur in couleurs_francais:
        langue = 'francais'
    elif couleur in couleurs_espagnol:
        langue = 'espagnol'
    else:
        langue = 'langue indéterminée'
        
    return langue

## Programme principal

resultat = trouve_langue_couleur('aul')
print(resultat)