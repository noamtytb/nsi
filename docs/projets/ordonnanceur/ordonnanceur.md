# Projet sur l'ordonnancement des processus à l'aide d'une file

## Description du travail à faire

Ce D.M. est à réaliser par trois. Les trinômes sont à me communiquer via l'ENT (Marette Amélie) **avant** le **Lundi 13 Décembre**. Mettre en copie M. Ramstein.

Le rendu final du projet est pour le **Lundi 3 janvier via l'ent.**

Un projet intermédiaire contenant au moins la première partie ainsi que le début du journal de bord est à rendre avant le **Jeudi 15 décembre**.

Un journal de bord est à tenir et à rendre au format texte ou pdf. Un fichier pour le groupe est suffisant. Il sera de la forme:

**Lundi 13 Décembre**

eleve 1 :

* tâches effectuées
* problèmes rencontrés
* solutions trouvées

eleve 2 :

* tâches effectuées
* problèmes rencontrés
* solutions trouvées

eleve 3 :

* tâches effectuées
* problèmes rencontrés
* solutions trouvées

Pour chacune des 3 parties créer un nouveau fichier python.

N'oubliez pas d'écrire vos noms et prénoms au début des fichiers.

Il est possible que vous ayez besoin de modules réalisés en classe. Dans ce cas stockez les dans le même dossier que vos fichiers python et importés les à l'aide de la commande `import nomdufichier`.

Pour chacun des rendus générés une archive contenant l'ensemble des fichiers appelée `prenom1_prenom2_prenom3.zip`.


## Description du projet

La plupart des ordonnanceurs modernes utilisent des files pour garder en mémoire de façon optimale les
programmes à exécuter:

- les programmes qui demandent du temps de calcul sont insérés en bout de file
- et ceux qui seront défilés pour obtenir  effectivement du "temps processeur " sont ceux qui attendent depuis le plus longtemps

Pour ce projet vous aurez donc besoin des classes `File` et `Maillon` écrites précédemment.


### L'ordonnancement classique

  1. créer une classe Activite pour modéliser des activités ayant trois attributs : nom, duree et priorite.
     On programmera les méthodes `__init__` et `repr` appropriées. La méthode `repr` renvoie sous forme de chaine de caractère le om la durée et la priorité de l'activité.

  2. créer une classe `Ordonnanceur` sur le patron suivant :

     ```python
     class Ordonnanceur:
     def __init__(self):
     self.file = File()
     def ajout_activite(self,activite):
     	# à remplir
     def step(self):
     	# à remplir
     def run(self):
     	# à remplir
     ```

     -  remplir la méthode `ajout_activite` qui ajoute une activité passée en paramètre à la file de processus de l’ordonnanceur

     -  remplir la méthode `step` qui effectue un "tour" d’ordonnancement comme suit : si la file est vide, on le dit et on ne fait rien.

     - s’il y a au moins une activité dans la file, on défile et on exécute l’activité  en attendant le temps correspondant à la durée de l’activité et en affichant son nom sa durée et sa priorité. La fonction `sleep(t)` du module `time` permet d'attendre `t` secondes.

     -  créer une liste de 10 activités de durée et de priorité aléatoires (durée entre 1 et 10 et priorité entre 0 et 2, la priorité n’étant de toute façon utilisée qu’en fin de TP). La fonction `randint(min,max)` du module `random` permet de générer aléatoirement un nombre compris entre `min` inclus et `max` inclus.

     - à l’aide d’une boucle, mettre toutes les activités dans la file de l’ordonnanceur puis exécuter l’ordonnanceur.

     - ajouter à `step` un tirage probabiliste qui ajoute une nouvelle activité aléatoire avec probabilité 3/10, puis exécuter à nouveau l’ordonnanceur sur un exemple.


### L'ordonnancement préemptif, la stratégie "round-robin"

L’ordonnancement à l’aide d’une file vu précédemment est dit coopératif : l’ordonnanceur laisse à chaque processus tout le temps dont il a besoin.

Dans une telle stratégie, c’est le processus qui doit rendre la main de lui-même, parce qu’il a terminé ou parce qu’il se met en attente (on n’a pas modélisé cette éventualité).

Dans les systèmes d’exploitation récents, l’ordonnancement est préemptif : au bout d’un certain temps, si le processus auquel est actuellement alloué le temps processeur n’a pas terminé, on le met en attente et on alloue le processeur à un autre processus.

1.  reprendre votre ordonnanceur pour qu’il prenne à l’initialisation un paramètre `tmax` dénotant le temps maximal laissé à un processus.

2. Modifier ensuite la méthode `step` pour qu’à chaque étape elle considère une activité et :

-  soit le temps d’exécution est inférieur à `tmax` et on exécute l’activité que l’on a défilée

-  soit ce temps d’exécution est strictement supérieur à `tmax`, alors on exécute l’activité défilée pendant `tmax`, puis on la renfile avec un temps d’exécution diminué de `tmax`


### L'ordonnancement multifile

Dans les systèmes d’exploitation modernes, il y a une notion de priorité : certains processus plus critiques doivent être exécutés avant les autres. On suppose ici que les priorités des activités sont des entiers entre 0 et 2 (0 est la plus haute priorité, 2 la plus faible). Une stratégie approchant ce qui se fait dans les systèmes modernes est la suivante :

- on crée une file pour chaque priorité 0, 1 et 2
- à l’ajout dans l’ordonnanceur, on ajoute l’activité à la bonne file
- la fonction `step` de l’ordonnanceur se comporte comme un round-robin, mais qui commence sur la file de priorité 0. Si elle est vide, il cherche une tâche dans la file de priorité 1. Si elle est vide, on regarde la file de priorité 2.

1. Implémenter un tel ordonnanceur et le faire tourner sur un exemple. N'oublier pas de gérer les cas tels que si une file est vide, par exemple celle de priorité 0, et qu’une nouvelle activité de priorité 0 est ajoutée.
