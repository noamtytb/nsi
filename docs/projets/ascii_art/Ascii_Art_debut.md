# Ascii Art

L’**art ASCII** consiste à réaliser des images uniquement à l'aide des lettres et caractères spéciaux contenus dans le code [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange).

Pour réaliser des dessins en ascii art en python nous allons utiliser des boucles ainsi que la fonction `print`.

## exemples simples en ascii art

Tester toutes les fonctions proposées.

### Le carré

```python
def carre(n):
    for i in range(n):
        ligne="@"*n
        print(ligne)
```

### Le triangle

```python
def triangle(n):
    for i in range(n):
        ligne="@"*(i+1)
        print(ligne)
```

### La diagonale

```python
def diagonale(n):
    for i in range(n):
        ligne=" "*i + "\\"
        print(ligne)
```

On remarque que pour obtenir le symbole `\` on a du mettre `\\`. En effet, le symbole \ seul est un caractère spécial de Python.
