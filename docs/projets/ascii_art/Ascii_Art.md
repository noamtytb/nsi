# Ascii Art

L’**art ASCII** consiste à réaliser des images uniquement à l'aide des lettres et caractères spéciaux contenus dans le code [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange).

Pour réaliser des dessins en ascii art en python nous allons utiliser des boucles ainsi que la fonction `print`.

## exemples simples en ascii art

Tester toutes les fonctions proposées.

### Le carré

```python
def carre(n):
    for i in range(n):
        ligne="@"*n
        print(ligne)
```

### Le triangle

```python
def triangle(n):
    for i in range(n):
        ligne="@"*(i+1)
        print(ligne)
```

### La diagonale

```python
def diagonale(n):
    for i in range(n):
        ligne=" "*i + "\\"
        print(ligne)
```

On remarque que pour obtenir le symbole `\` on a du mettre `\\`. En effet, le symbole \ seul est un caractère spécial de Python.


## Quelques défis

Ecrire les fonctions qui permettent d'obtenir les figures proposées.

## défi 1

```python
triangle2(3)

@@@
@@
@
```

## défi 2

```python
carre_creux(4)

####
#  #
#  #
####
```

On s'aidera du modèle ci-dessous.

```python
def carre_creux(n):
    ligne='#'*......
    print(..............)
    for i in range(.......):
        ligne=..........
        print(.........)
    ligne=...........
    print(...........)
```


## défi 3

```python
triangle3(3)

@@@
 @@
  @
```

On s'aidera du modèle ci-dessous.

```python
def triangle3(n):
    for i in range(n):
        ligne=.............
        print(..........)
```


## défi 4

```python
diagonale2(3)
  /
 /
/  
```


## défi 5

```python
pyramide(3)

  @
 @@@
@@@@@

pyramide(4)

   @
  @@@
 @@@@@
@@@@@@@
```

## défi 6

```python
losange(3)

  @
 @@@
@@@@@
 @@@
  @

losange(4)

   @
  @@@
 @@@@@
@@@@@@@
 @@@@@
  @@@
   @
```


## défi 7

Ecrire une fonction `arbre(diametre, hauteur)` qui permet d'obtenir la figure suivante :

```python
arbre(7,10)

$$$$$$$
$$$$$$$
$$$$$$$
$$$$$$$
$$$$$$$
$$$$$$$
$$$$$$$
  | |
  | |
  | |

arbre(8,10)

$$$$$$$$
$$$$$$$$
$$$$$$$$
$$$$$$$$
$$$$$$$$
$$$$$$$$
$$$$$$$$
$$$$$$$$
   ||
   ||
```

Les arbres suivent le cahier des charges suivant :

- les feuilles sont des `$`
- la taille du feuillage est une carré de la taille du diamètre
- le tronc:
     - si le diamètre est pair : un tronc à `diamètre / 2` et un tronc à `diamètre / 2 + 1` (deux troncs à la suite)
     - si le diamètre est impair : un tronc à `diamètre / 2` suivi d’un espace et de nouveau un tronc
- la taille du tronc est égale à `hauteur - diamètre`


## défi 8

Ecrivez une fonction `sapin(diametre, hauteur)` qui permet d'obtenir la figure suivante:

```python
sapin(9,12)

         $
        $$$
       $$$$$
      $$$$$$$
     $$$$$$$$$
    $$$$$$$$$$$
   $$$$$$$$$$$$$
  $$$$$$$$$$$$$$$
 $$$$$$$$$$$$$$$$$
         |
         |
         |
```


## défi 9

A l'aide de la fonction du défi 7, écrivez une fonction `arbre_fruitier(diametre, hauteur)` qui permet d'obtenir la figure suivante:

```python
$$$oo$$
$$$$$$$
$$$$$$$
$$$$$$$
$o$$ooo
$$$$$o$
$$$$$$$
  | |
  | |
  | |
```

Les "o" sont ajoutés aléatoirement avec une probabilité de `0.3`. Pour cela, utilisez la fonction `randint` du module `random`.

```python
if random.randint(0, 9) < 3:
    instruction1
```


## défi 10

A l'aide de la fonction du défi 8, écrivez une fonction `sapin_noel(diametre, hauteur)` qui permet d'obtenir la figure suivante:

```python
   		$
       $$o
      $$$$$
     $$$$$$$
    o$$$**$*$
   o$$$$*$$$$$
  *$$$o$$$$$$$o
 $$$$$$o$$$$$$$$
        |
        |
        |
        |
```

Des boules de noël `o` et des étoiles `*` sont ajoutées aléatoirement au sapin avec une probabilité de `0.3`.


## défi 11

Le but de ce défi est d'afficher le graphique de la fonction mathématique `sinus`.

1. Complétez le code de la fonction suivante qui affiche la liste du résultat de la fonction `sinus` entre `0` et `1.5` avec un pas de `0.1`.

```python
import math

def sinpi():
   for i in range(0, 15):
       valeur = math.sin( ... * 0.2)
       print(...)
```

2. Servez-vous de l'exemple précédent pour écrire une fonction `affichage_classique(l)` qui permet d'obtenir la figure suivante :

```python
affichage_classique(30)

======
============
=================
======================
=========================
============================
==============================
==============================
=============================
===========================
========================
====================
===============
==========
```

Cette figure est la représentation de la fonction `sin(x)` pour `x` entre `0` et `pi`.

On remplace chaque valeur obtenue dans la liste par une barre horizontale de longueur proportionnelle à la valeur. On utilisera l'opérateur `*` des chaînes de caractères et la fonction `round(y)` qui retourne le nombre entier le plus proche de `y`.

3.  Un problème de cette fonction est qu'elle ne travaille pas avec les nombres négatifs.

écrivez une fonction `affichage_negatif(l)` qui permet d'obtenir la figure suivante pour `sin(x)` avec `x` compris entre `0` et `2pi`.

```python
 							                ======
                              ============
                              =================
                              ======================
                              =========================
                              ============================
                              ==============================
                              ==============================
                              =============================
                              ===========================
                              ========================
                              ====================
                              ===============
                              ==========
                              ====
                            ==
                      ========
                 =============
            ==================
       =======================
    ==========================
 =============================
==============================
==============================
 =============================
   ===========================
       =======================
           ===================
                ==============
```

L'affichage respecte les règles suivantes:

- si la valeur à afficher est positive, afficher 30 espaces suivi de la barre
- si la valeur à afficher est négative, afficher un nombre d'espace égale à 30 moins la longueur de la barre, suivi de la barre

4. Reprenez le code de `affiche_negatif(l)` et écrivez le fonction `affiche_étoile(l)` qui affiche la figure suivante pour `sin(x)` avec `x` compris entre `0` et `2pi`.

```python
                              *
                                   *
                                         *
                                              *
                                                   *
                                                      *
                                                         *
                                                           *
                                                           *
                                                          *
                                                        *
                                                     *
                                                 *
                                            *
                                       *
                                 *
                            *
                      *
                 *
            *
       *
    *
 *
*
*
 *
   *
       *
           *
                *
```
