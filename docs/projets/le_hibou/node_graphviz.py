#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Installer graphviz et son module python.

La classe Node est celle vue en cours. La méthode affiche_arbre génère un fichier pdf contenant l'arbre.

Le fichier est ouvert automatiquement. Il faut le fermer avant de relancer un affichage sinon une erreur s'affichera.
'''

## Importation des modules

import graphviz


## Déclaration des constantes


## Déclaration des classes

class Node:
    '''
    Classe Node vue en cours avec une méthode d'affichage d'arbre.
    '''
    def __init__(self, value, left=None, right=None):
        '''
        Constructeur.
        
        @param_entree : value, étiquette du noeud.
        @param_entree : left, objet Node, noeud racine du sous arbre de gauche.
        @param_entree : right, objet Node, noeud racine du sous arbre de droite.
        @param_sortie : aucun.
        '''
        self.value = value
        self.left = left
        self.right = right

    def affiche_arbre(self):
        '''
        Affiche l'arbre en utilisant le module graphviz.
        
        @param_entree : aucun.
        @param_sortie : aucun.
        '''
        def parcours(noeud):
            '''
            Parcours l'arbre à partir de noeud et en contruisant l'arbre graphviz.
            
            @param_entree : noeud, objet Node, noeud racine de l'arbre à parcourir.
            @param_sortie : aucun.
            '''
            graphe.node(str(noeud.value), str(noeud.value))
            if noeud.left is not None:
                graphe.node(str(noeud.left.value), str(noeud.left.value))
                graphe.edge(str(noeud.value), str(noeud.left.value))
                parcours(noeud.left)
            else:
                graphe.attr('node', shape='none')
                graphe.node(str(noeud.value) + "_Blank_left", "")
                graphe.edge(str(noeud.value), str(noeud.value) + "_Blank_left")
                graphe.attr('node', shape='ellipse')
            if noeud.right is not None:
                graphe.node(str(noeud.right.value), str(noeud.right.value))
                graphe.edge(str(noeud.value), str(noeud.right.value))
                parcours(noeud.right)
            else:
                graphe.attr('node', shape='none')
                graphe.node(str(noeud.value) + "_Blank_right", "")
                graphe.edge(str(noeud.value), str(noeud.value) + "_Blank_right")
                graphe.attr('node', shape='ellipse')

        graphe = graphviz.Graph('arbre_binaire')
        parcours(self)
        graphe.view()


## Déclaration des fonctions


## Fonction principale


## Programme principal

arbre = Node('A', Node('B'), Node('C', Node('D')))
arbre.affiche_arbre()