## Somme des entiers de 0 à n

**Algorithme**
```
@param_entree : n, nombre entier
@param_sortie : accumulateur, nombre entier

▪ Pour chaque valeur, de 0 à n :
    ▪ Ajouter la valeur à un accumulateur
```
<br>

**Formalisation avec noms de variables**
```
@param_entree : n, nombre entier
@param_sortie : accumulateur, nombre entier

▪ accumulateur ← 0
▪ Pour i allant de 0 à n :
    ▪ accumulateur ← accumulateur + i
```


## Somme des valeurs des éléments d'un tableau

**Algorithme**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : accumulateur, nombre entier ou réel

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Ajouter la valeur de l'élément à un accumulateur
```
<br>

**Formalisation avec noms de variables**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : accumulateur, nombre entier ou réel

▪ accumulateur ← 0
▪ n ← longueur du tableau
▪ Pour i allant de 0 à n - 1 :
    ▪ accumulateur ← accumulateur + tableau[i]
```


## Moyenne des valeurs des éléments d'un tableau

**Algorithme**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : moyenne, nombre entier ou réel

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Ajouter la valeur de l'élément à un accumulateur
▪ Calculer la moyenne en divisant la valeur de l'accumulateur par le nombre d'éléments du tableau
```
<br>

**Formalisation avec noms de variables**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : moyenne, nombre entier ou réel

▪ somme ← 0
▪ n ← longueur du tableau
▪ Pour i allant de 0 à n - 1 :
    ▪ somme ← somme + tableau[i]
▪ moyenne ← somme / n
```


## Recherche du minimum

**Algorithme**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : valeur_minimum

▪ n ← longueur du tableau
▪ valeur_minimum ← tableau[0]
▪ Pour i allant de 1 à n - 1 :
    ▪ Si tableau[i] < valeur_minimum :
        ▪ valeur_minimum ← tableau[i]
```


## Recherche du maximum

**Algorithme**
```
@param_entree : tableau, tableau de nombres entiers ou réels
@param_sortie : valeur_maximum

▪ n ← longueur du tableau
▪ valeur_maximum ← tableau[0]
▪ Pour i allant de 1 à n - 1 :
    ▪ Si tableau[i] > valeur_maximum :
        ▪ valeur_maximum ← tableau[i]
```


## Recherche de la première occurrence d'un élément

**Algorithme**
```
@param_entree : tableau
@param_entree : element
@param_sortie : position

▪ n ← longueur du tableau
▪ i ← 0
▪ position ← Aucune
▪ Tant que i < n et position = Aucune :
    ▪ Si tableau[i] = element:
        ▪ position ← i
    ▪ Sinon :
        ▪ i ← i + 1
```


## Recherche de toutes les occurrences d'un élément

**Algorithme**
```
@param_entree : tableau, tableau d'éléments
@param_entree : element
@param_sortie : positions, tableau de nombres entiers

▪ n ← longueur du tableau
▪ positions ← tableau vide
▪ Pour i allant de 0 à n - 1 :
    ▪ Si tableau[i] = element:
        ▪ Concaténer i à positions
```


## Multiplication de a par b

**Algorithme**
```
@param_entree : a, nombre entier
@param_entree : b, nombre entier
@param_sortie : c, nombre entier

▪ c ← 0
▪ Pour i allant de 0 à b - 1 :
    ▪ c ← c + a
```


## Division de a par b

**Algorithme**
```
@param_entree : a, nombre entier
@param_entree : b, nombre entier
@param_sortie : q, nombre entier
@param_sortie : r, nombre entier

▪ r ← a
▪ q ← 0
▪ Tant que r - b > 0 :
    ▪ r ← r - b
    ▪ q ← q + 1
```


## Insertion

**Algorithme**
```
@param_entree : liste
@param_entree : valeur_a_inserer
@param_entree : position
@param_sortie : liste (modifiée)

ajouter None à la fin de liste
n ← longueur de liste

i ← n - 1
BOUCLE tant que i >= position:
    liste[i] ← liste[i - 1]
    i ← i - 1
liste[position] ← valeur_a_inserer
```


## Tri par sélection

**Principe de l'algorithme**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Chercher l'élément le plus petit de la liste à partir de l'élément en cours jusqu'à la fin du tableau
    ▪ Echanger l'élément en cours avec l'élément le plus petit
```
<br>

**Développement de la deuxième boucle**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Stocker la position de l'élément en cours
    ▪ Pour chaque élément du tableau, de l'élément en cours au dernier :
        ▪ Si l'élément est plus petit que l'élément à la position stockée :
            ▪ Stocker la nouvelle position
    ▪ Echanger l'élément en cours avec l'élément à la position stockée
```
<br>

**Formalisation avec noms de variables**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ n ← longueur du tableau
▪ Pour i allant de 0 à n - 1 :
    ▪ position_plus_petit ← i
    ▪ Pour j allant de i à n - 1 :
        ▪ Si tableau[j] < tableau[position_plus_petit] :
            ▪ position_plus_petit ← j
    ▪ temp ← tableau[i]
    ▪ tableau[i] ← tableau[position_plus_petit]
    ▪ tableau[position_plus_petit] ← temp
```


## Tri par insertion

**principe de l'algorithme**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Insérer l'élément en cours à sa place entre le premier élément et l'élément en cours
```
<br>

**Développement de la deuxième boucle**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ Pour chaque élément du tableau, du premier au dernier :
    ▪ Stocker l'élément en cours
    ▪ Pour chaque élément du tableau, de l'élément en cours au premier
        ▪ Si l'élément précédent est plus grand que l'élément stocké :
            ▪ Copier l'élément précédent à la place de l'élément
        ▪ Sinon :
            ▪ Copier l'élément stocké à la place de l'élément
            ▪ Arrêter la boucle
```
<br>

**Formalisation avec noms de variables**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ n ← longueur du tableau
▪ Pour i allant de 0 à n - 1 :
    ▪ element_a_inserer ← tableau[i]
    ▪ Pour j allant de i à 0 :
        ▪ Si tableau[j - 1] > element_a_inserer :
            ▪ tableau[j] ← tableau[j - 1]
        ▪ Sinon :
            ▪ tableau[j] ← element_a_inserer
            ▪ Arrêter la boucle
```
<br>

**Utilisation d'une boucle tant que**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ n ← longueur du tableau
▪ Pour i allant de 0 à n - 1 :
    ▪ element_a_inserer ← tableau[i]
    ▪ j ← i
    ▪ Tant que j > 0 et tableau[j - 1] > element_a_inserer :
        ▪ tableau[j] ← tableau[j - 1]
        ▪ j ← j - 1
    ▪ tableau[j] ← element_a_inserer
```
<br>

**Optimisation en démarrant à partir du deuxième élément**
```
@param_entree : tableau non trié
@param_sortie : tableau trié

▪ n ← longueur du tableau
▪ Pour i allant de 1 à n - 1 :
    ▪ element_a_inserer ← tableau[i]
    ▪ j ← i
    ▪ Tant que j > 0 et tableau[j - 1] > element_a_inserer :
        ▪ tableau[j] ← tableau[j - 1]
        ▪ j ← j - 1
    ▪ tableau[j] ← element_a_inserer
```


## Recherche dichotomique

**Algorithme**

```
@param_entree : liste (triée)
@param_entree : element_recherche
@param_sortie : position


n ← longueur de liste

debut_sous_liste ← 0
fin_sous_liste ← n - 1
position ← Rien
trouve ← Faux
BOUCLE tant que trouve = Faux et debut_sous_liste <= fin_sous_liste:
    i_milieu ← (debut_sous_liste + fin_sous_liste) // 2
    SI liste[i_milieu] < element_recherche:
        debut_sous_liste ← i_milieu + 1
    SINON SI liste[i_milieu] > element_recherche:
        fin_sous_liste ← i_milieu - 1
    SINON SI liste[i_milieu] = element_recherche:
        trouve ← Vrai
        position ← i_milieu
```


<!--
recherche dichotomique récursive
Parité
Divisible par deux
Nombres de chiffres dans un nombre
PGCD
fusion
Tri fusion
tri à bulle
les autres tris ?
-->
