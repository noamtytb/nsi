# Ressources NSI - Outils


## Utilisation d'un drone Tello avec Python

Les drones Tello peuvent être commandés à partir de Python à travers une liaison WIFI.

* Fiche d'utilisation du drone avec Python : [pdf](./drone_tello/fiche_utilisation_tello.pdf) ou [odt](./drone_tello/fiche_utilisation_tello.odt)
* Documentation du Tello SDK 1.3 : [pdf](./drone_tello/Tello_SDK_Documentation_EN_1.3.pdf)
* Documentation du Tello EDU SDK 2.0 : [pdf](./drone_tello/Tello_SDK_2.0_User_Guide.pdf)
* Fichier Python exemple : [py](./drone_tello/Tello3.py)
