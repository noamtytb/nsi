#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Voiture():
    """Une classe définissant un modèle de voiture"""

    def __init__(self, moteur, couleur, puissance):
        self.moteur = moteur
        self.couleur = couleur
        self.puissance = puissance

    def caracteristiques(self):
        return {"moteur": self.moteur, "couleur": self.couleur, "puissance": self.puissance}

    def modifie_couleur(self, couleur):
        self.couleur = couleur

    def __del__(self):
        print("objet détruit")


voiture_1 = Voiture('diesel', 'rouge', 130)
voiture_2 = Voiture('essence', 'jaune', 160)
voiture_1.couleur = 'vert'
voiture_1.modifie_couleur('violet')
print(voiture_1.couleur)
print(voiture_1.caracteristiques())
del(voiture_1)
