# Ressources NSI - Outils


## Prise en main de la rédaction en markdown


### Editeurs

Le markdown est un langage de balises permettant de créer des documents légers avec une mise en forme minimale. Ces documents peuvent être publiés directement sur de nombreux services web comme les forges GitLab ou GitHub.

1\. Utiliser un éditeur de texte orienté markdown en ligne :

- **StackEdit** : [lien](https://stackedit.io/)

2\. Ou utiliser un éditeur local comme :

- **Ghostwriter** : [zip](./logiciels/ghostwriter_x64_portable.7z)
- **Notepad ++** avec l'extension **MarkdownViewer ++** : [zip](./logiciels/notepad++_with_markdown.zip)
- **Atom** avec les extensions **markdown-preview-enhanced** et **markdown-image_assistant** : [lien](https://atom.io/)

3\. Dans l'éditeur rédiger les documents sauvegardés avec l'extension **.md**.

4\. La plupart des éditeurs permettent de prévisualiser le document pendant sa rédaction.

5\. La plupart des éditeurs permettent d'exporter le document au format **.html** ou **.pdf**.


### Quelques éléments de syntaxe

Le markdown est très simple d'utilisation. Il existe bon nombre de tutoriels. Pour une initiation simple et claire on peut se référer au tutoriel du site ionos : [lien](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/).

La mise en forme du texte, l'insertion de listes, de tableaux, d'images, de liens et de codes sont standardisés et communs à tous les éditeurs et sites de publication. Par contre l'insertion de tableaux évolués, de codes évolués, d'équations, ..., peut s'avérer spécifique.

Enfin la syntaxe **HTML** est utilisable dans un fichier markdown. Par exemple souligner du texte n'existe pas en markdown mais on peut utiliser la balise **<u\>** pour obtenir ceci : <u>texte souligné</u>. Pour insérer des lignes vierges on utilisera la balise **<br\>**.

 <u>Attention toutefois</u> : trop utiliser de balises **HTML** revient finalement à écrire une page web en perdant l'intérêt d'une rédaction légère markdown.
