# Ressources NSI - Outils


## Les quaternions

Les quaternions sont une généralisation des nombres complexes aux trois dimensions de l'espace. En informatique ils sont utilisés pour la rotation d'objets en trois dimensions.

* Fiche de cours : [pdf](./quaternions/quaternions.pdf) ou [odt](./quaternions/quaternions.odt)
* Fiche de TP : [pdf](./quaternions/TP_quaternions_python.pdf) ou [odt](./quaternions/TP_quaternions_python.odt)
