# Ressources NSI - Outils


## Modules python

Modules Python d'intérêt et activités pour les prendre en main.

* L'interface graphique **Tkinter** : [pdf](./interfaces_graphiques/interfaces_graphiques_introduction.pdf) ou [odt](./interfaces_graphiques/interfaces_graphiques_introduction.odt)
* Animation avec une interface graphique **Tkinter** : [pdf](./interfaces_graphiques/tkinter_animation.pdf) ou [odt](./interfaces_graphiques/tkinter_animation.odt)
* Utilisation basique de la **Python Imaging Library** : [pdf](./PIL_pillow/PIL_pillow.pdf) ou [odt](./PIL_pillow/PIL_pillow.odt)
